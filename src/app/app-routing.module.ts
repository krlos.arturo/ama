import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IsMobileService } from './guards/isMobile/is-mobile.service';
import { LoggedGuard } from './guards/logged/logged.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'app',
    pathMatch: 'full'
  },
  { path: 'web', loadChildren: './web/app.module#AppPageModule' },
  {
    path: 'app',
    loadChildren: './mobile/app.module#AppPageModule',
    canActivate: [IsMobileService]
  },
  { path: 'perfil-eps', loadChildren: './mobile/pages/perfil-eps/perfil-eps.module#PerfilEpsPageModule',canActivate: [LoggedGuard] },
  { path: 'municipio-depa', loadChildren: './pages/municipio-depa/municipio-depa.module#MunicipioDepaPageModule' ,canActivate: [LoggedGuard]},
  { path: 'selecciondepartamental', loadChildren: './pages/selecciondepartamental/selecciondepartamental.module#SelecciondepartamentalPageModule',canActivate: [LoggedGuard] },
  { path: 'comentarios/:clinica-cod', loadChildren: './mobile/pages/comentarios/comentarios.module#ComentariosPageModule' },
  { path: 'reportes', loadChildren: './mobile/pages/reportes/reportes.module#ReportesPageModule' },
  { path: 'comentario/:clinica', loadChildren: './mobile/pages/comentario/comentario.module#ComentarioPageModule' },
  { path: 'clinicas-favoritas/:id/:clinicaco', loadChildren: './mobile/pages/clinicas-favoritas/clinicas-favoritas.module#ClinicasFavoritasPageModule' }




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
