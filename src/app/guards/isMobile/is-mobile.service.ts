import { Injectable, NgZone } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { isDevMode } from '@angular/core';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class IsMobileService implements CanActivate{
  
  constructor(private router: Router, private ngZone: NgZone, private platform: Platform) { }

  canActivate(route: ActivatedRouteSnapshot): boolean{
    
    if( this.platform.is('mobile') && !this.platform.is('mobileweb') || isDevMode() ){
      return true;
    }else{
      this.ngZone.run(()=>{
        this.router.navigate(['web'], {
          replaceUrl: true
        });
      });
      return false;
    }
  }
}
