import { TestBed, async, inject } from '@angular/core/testing';

import { LoggedToWebGuard } from './logged-to-web.guard';

describe('LoggedToWebGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoggedToWebGuard]
    });
  });

  it('should ...', inject([LoggedToWebGuard], (guard: LoggedToWebGuard) => {
    expect(guard).toBeTruthy();
  }));
});
