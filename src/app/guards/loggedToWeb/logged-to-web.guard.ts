import { Injectable, NgZone } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable({
  providedIn: 'root'
})
export class LoggedToWebGuard implements CanActivate {
  
  constructor(private fireAuth: AngularFireAuth, private router: Router, private ngZone: NgZone){

  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    return new Promise((resolve, reject) =>{
      this.fireAuth.auth.onAuthStateChanged((user: firebase.User) => {
        if (user) {
          resolve(true);
        } else {
          this.ngZone.run(()=>{
            this.router.navigate(['/web/login'], {
              replaceUrl: true
            });
          });
          resolve(false);
        }
      });  
    });
  }
  
}
