import { Injectable, NgZone } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseAuth, FirebaseApp } from 'angularfire2';

@Injectable({
  providedIn: 'root'
})
export class LoggedGuard implements CanActivate {
  
  constructor(private router: Router, private fireAuth: AngularFireAuth, private ngZone: NgZone){
    
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): boolean | Observable<boolean> | Promise<boolean> {
    
    return new Promise((resolve, reject) =>{
      this.fireAuth.auth.onAuthStateChanged((user: firebase.User) => {
        if (user) {
          resolve(true);
        } else {
          this.ngZone.run(()=>{
            this.router.navigate(['/app/login'], {
              replaceUrl: true
            });
          });
          resolve(false);
        }
      });  
    });
    
    this.router.navigateByUrl('/app/login', {
      replaceUrl: true
    });
    
  }
    

}
