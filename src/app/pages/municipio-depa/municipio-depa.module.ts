import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MunicipioDepaPage } from './municipio-depa.page';

const routes: Routes = [
  {
    path: '',
    component: MunicipioDepaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MunicipioDepaPage]
})
export class MunicipioDepaPageModule {}
