import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-selecciondepartamental',
  templateUrl: './selecciondepartamental.page.html',
  styleUrls: ['./selecciondepartamental.page.scss'],
})
export class SelecciondepartamentalPage implements OnInit {
  activadorMunicipios: number;
  public departamentos;
  departamento;
  uid;
  activador;
  municipio: any;
  constructor(public http:HttpClient,public db:AngularFirestore,public auth:AngularFireAuth,public modalController:ModalController) { 
    this.auth.auth.onAuthStateChanged(k=>{
      this.uid=k.uid
    })
  }
  
  ngOnInit() {
  }
  async obtenerDepartamentos(code){
   console.log(code)
   console.log(this.departamento)
     
    
     
     
        this.activadorMunicipios=1
        
       
          this.http.get("https://www.datos.gov.co/resource/xdk5-pm3f.json?c_digo_dane_del_departamento="+this.departamento).subscribe(k=>{
          
          console.log(k)
      
        this.departamentos=k
    
          
         console.log(this.departamentos)
        })
   
  }


  async registrarDepartamentalizacion(){
    this.db.collection('usuarios').doc(this.uid).update({
      departamento:this.departamento,
      municipio:this.municipio
    }).then(()=>{
      this.modalController.dismiss()
    })
  }
}
