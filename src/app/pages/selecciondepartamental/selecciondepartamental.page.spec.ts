import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelecciondepartamentalPage } from './selecciondepartamental.page';

describe('SelecciondepartamentalPage', () => {
  let component: SelecciondepartamentalPage;
  let fixture: ComponentFixture<SelecciondepartamentalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelecciondepartamentalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelecciondepartamentalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
