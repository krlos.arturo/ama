import { DocumentReference } from '@angular/fire/firestore';

export interface PermissionValue {
  module: DocumentReference;
  list: DocumentReference[];
}

export interface Permission {
  [key: string]: PermissionValue;
}

export interface Role {
  created_at: Date;
  description?: string;
  name: string;
  permissions: Permission;
  state: string;
}
