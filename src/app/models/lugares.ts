import { LugarTipo } from './lugares-tipos';

export interface Lugar {
    direccion: string;
    estado: number;
    imagen_perfil: string;
    nombre: string;
    ubicacion: string;
    tipo_lugar?: any;
}