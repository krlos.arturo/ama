export interface Usuario {
    apellido: string;
    correo: string;
    direccion: string;
    estado: number;
    imagen_perfil: string;
    nombre: string;
    usuario: string;
    departamento:string,
    municipio:string,

    eps:string
   comentarios:any
}