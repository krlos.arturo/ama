import { DocumentReference } from '@angular/fire/firestore';

export interface Module {
  code: string;
  created_at: Date;
  name: string;
  state: string;
  uuid?: string;
  doc?: DocumentReference;
}
