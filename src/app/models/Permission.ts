import { DocumentReference } from '@angular/fire/firestore';

export interface Permission {
  code: string;
  created_at: Date;
  description?: string;
  doc?: DocumentReference;
  checked?: boolean;
  name: string;
  state: string;
  uuid?: string;
}
