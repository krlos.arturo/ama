export interface usuarios{
    apellido:string,
    
    correo:string,

    direccion:string,
    
    estado:number,

    imagen_perfil:string,

    nombre:string,

    comentarios:any[],

    oneSigID:string,

    eps:string

    
}