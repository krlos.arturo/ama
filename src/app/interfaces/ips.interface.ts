export interface IPS{
    fechas:string,
    nombres:string,
    tipos:string,
    direcciones:string,
    latitudes:any,
    longitudes:any,
    telefonos:string,
    niveles:string,
    nities:string,
    municipioIPSs:string,
    departamentoIPSs:string
}