export interface Marcador{
  
    position:{
        lat:number,
        lng:number
    },
    uid:string,
   
    title:string,
    icon:string
}