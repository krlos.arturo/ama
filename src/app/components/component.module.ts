import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartamentosComponent } from './departamentos/departamentos.component';
import { ComentarioComponent } from './comentarios/comentario/comentario.component';
import { FormsModule } from '@angular/forms';
import { ClinicasFavoritasComponent } from './clinicas-favoritas/clinicas-favoritas.component';

@NgModule({
  declarations: [DepartamentosComponent,ComentarioComponent,ClinicasFavoritasComponent],
  imports: [
    CommonModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  exports:[DepartamentosComponent,ComentarioComponent,ClinicasFavoritasComponent]
})
export class ComponentModule { }
