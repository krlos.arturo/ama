import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { ComentarioService } from 'src/app/services/comentarios/comentario.service';

@Component({
  selector: 'app-comentario',
  templateUrl: './comentario.component.html',
  styleUrls: ['./comentario.component.scss'],
})

export class ComentarioComponent implements OnInit {

  valorEstado
  texto
  constructor(private modalController:ModalController,private db:AngularFirestore,private comentarioService:ComentarioService) { }

  ngOnInit() {}
  async salir(){
    this.modalController.dismiss()
    

  }

  


  asignarValorEstado(valor){
    this.valorEstado=valor
    console.log(this.valorEstado)
  }
  async realizarComentario(){
    console.log(this.texto)

  }
}
