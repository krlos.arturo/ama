import { Component, OnInit } from '@angular/core';
import { NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
declare var google;
@Component({
  selector: 'app-clinicas-favoritas',
  templateUrl: './clinicas-favoritas.component.html',
  styleUrls: ['./clinicas-favoritas.component.scss'],
})
export class ClinicasFavoritasComponent implements OnInit {
  map: any;
  bounds: any;

  constructor() { }

  ngOnInit() {
    
      //Geocoder
      let options: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
    };
      // create a new map by passing HTMLElement
      const mapEle: HTMLElement = document.getElementById('map');
      // create LatLng object
   
   
      const myLatLng ={lat:4.4432782,lng:-75.2424638};
      // create map
      this.map = new google.maps.Map(mapEle, {
        center:myLatLng,
        disableDefaultUI: true,
        draggable: true,
        
        zoom: 12
      });
  /**/ 
        
  this.bounds = new google.maps.LatLngBounds();
  
   /**/   
      google.maps.event.addListenerOnce(this.map, 'idle', () => {
  
        
       
        mapEle.classList.add('show-map');
      });
  }

}
