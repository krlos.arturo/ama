export const loginErrorMessages = {
    "auth/invalid-email"    : "El email ingresado no es Valido",
    "auth/user-disabled"    : "No puedes acceder en este momento porqué tu usuario se ha desactivado",
    "auth/user-not-found"   : "No pareces estar registrado, considera crear una cuenta",
    "auth/wrong-password"   : "La contraseña que ingresaste no es correcta"
}

export const resgistroConEmailErrorMessages = {
    "auth/email-already-in-use"     : "Este Email ya ha sido tomado,¿has olvidado tu contraseña?",
    "auth/invalid-email"            : "El email ingresado no es Valido",
    "auth/operation-not-allowed"    : "Tu correo esta desactivado en la plataforma, no puedes acceder",
    "auth/weak-password"            : "Tu contraseña es demasiado debil"
}

export const restablecimientoPassErrorMessages = {
    "auth/user-not-found"   : "No pareces estar registrado, considera crear una cuenta",
    "auth/invalid-email"    : "El email ingresado no es Valido",
}