import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AppPage } from './app.page';
import { LoggedToWebGuard } from '../guards/loggedToWeb/logged-to-web.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: '../web/pages/login/login.module#LoginPageModule'
  },
  {
    path: '',
    component: AppPage,
    //canActivate: [LoggedToWebGuard],
    children: [
      {
        path: 'home',
        loadChildren: '../web/pages/home/home.module#HomePageModule'
      },
      //Rutas relacionadas con los lugares
      {
        path: 'lugares',
        loadChildren: '../web/pages/lugares/lugares.module#LugaresPageModule'
      },
      {
        path: 'lugares/acciones',
        loadChildren:
          '../web/pages/lugares/acciones/acciones.module#AccionesPageModule'
      },
      {
        path: 'lugares/acciones/:id',
        loadChildren:
          '../web/pages/lugares/acciones/acciones.module#AccionesPageModule'
      },
      //Rutas relacionadas con los tipos lugares
      {
        path: 'lugares/tipos',
        loadChildren:
          '../web/pages/lugares-tipos/lugares-tipos.module#LugaresTiposPageModule'
      },
      {
        path: 'lugares/tipos/acciones',
        loadChildren:
          '../web/pages/lugares-tipos/acciones/acciones.module#AccionesPageModule'
      },
      {
        path: 'lugares/tipos/acciones/:id',
        loadChildren:
          '../web/pages/lugares-tipos/acciones/acciones.module#AccionesPageModule'
      },
      //Rutas relacionadas con los usuarios
      {
        path: 'usuarios',
        loadChildren: '../web/pages/usuarios/usuarios.module#UsuariosPageModule'
      },
      {
        path: 'usuarios/acciones',
        loadChildren:
          '../web/pages/usuarios/acciones/acciones.module#AccionesPageModule'
      },
      {
        path: 'usuarios/acciones/:id',
        loadChildren:
          '../web/pages/usuarios/acciones/acciones.module#AccionesPageModule'
      },
      //Rutas relacionadas con los usuarios
      {
        path: 'roles',
        loadChildren: '../web/pages/roles/roles.module#RolesPageModule'
      },
      {
        path: 'roles/actions',
        loadChildren:
          '../web/pages/roles/actions/actions.module#ActionsPageModule'
      },
      {
        path: 'roles/actions/:id',
        loadChildren:
          '../web/pages/roles/actions/actions.module#ActionsPageModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AppPage]
})
export class AppPageModule {}
