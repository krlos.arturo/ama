import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

//Servicio de crud usuarios
import { RolesService } from 'src/app/services/roles/roles.service';

//Modelo usuarios
import { Role } from 'src/app/models/Role';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.page.html',
  styleUrls: ['./roles.page.scss']
})
export class RolesPage implements OnInit {
  roles: Observable<Role[]>;

  constructor(private rolesService: RolesService) {}

  ngOnInit() {
    this.roles = this.rolesService.getRoles();
  }
}
