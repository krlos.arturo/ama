import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

//Servicio de crud usuarios
import { RolesService } from 'src/app/services/roles/roles.service';
import { ModulesService } from 'src/app/services/modules/modules.service';
import { PermissionsService } from 'src/app/services/permissions/permissions.service';

//Modelo usuarios
import { Role, Permission as RolePermission } from 'src/app/models/Role';
import { Module } from 'src/app/models/Module';
import { Permission } from 'src/app/models/Permission';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.page.html',
  styleUrls: ['./actions.page.scss']
})
export class ActionsPage implements OnInit {
  role: Role = {
    state: 'active',
    name: '',
    description: '',
    created_at: new Date(),
    permissions: {}
  };
  moduleData: Module;
  permissionsData: Permission[];
  modulePermissions: any = {};
  uuid: string;
  error: string;
  modules: Observable<Module[]>;
  permissions: Permission[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private navController: NavController,
    private rolesService: RolesService,
    private loadingController: LoadingController,
    private modulesService: ModulesService,
    private permissionsService: PermissionsService
  ) {}

  ngOnInit() {
    this.uuid = this.activatedRoute.snapshot.params.id;
    this.loadModules();
    this.loadPermissions();
    if (this.uuid) {
      this.loadRole();
    }
  }

  selectPermissions(event, value: Permission, checked: boolean) {
    event.preventDefault();
    event.stopPropagation();

    if (
      this.uuid &&
      this.modulePermissions[this.moduleData.code] &&
      this.modulePermissions[this.moduleData.code].list
    ) {
      this.modulePermissions[this.moduleData.code].list =
        this.modulePermissions[this.moduleData.code].list ||
        this.role.permissions[this.moduleData.code].list;
    } else {
      console.log(
        '>>>> this.modulePermissions[this.moduleData.code]',
        this.modulePermissions[this.moduleData.code], this.modulePermissions[this.moduleData.code]
      );
      if (checked) {
        this.modulePermissions[this.moduleData.code] = {
          module: this.moduleData.doc,
          list: this.modulePermissions[this.moduleData.code].list || []
        };

        this.modulePermissions[this.moduleData.code].list.push(value.doc);
      } else {
        this.modulePermissions[this.moduleData.code].list.forEach(
          (modulePermission, i) => {
            modulePermission
              .get()
              .then((doc) => {
                const newPermission = doc.data();

                if (newPermission.uuid === value.uuid) {
                  if (
                    !this.modulePermissions[this.moduleData.code].list ||
                    (this.modulePermissions[this.moduleData.code].list &&
                      this.modulePermissions[this.moduleData.code].list
                        .length) <= 0
                  ) {
                    delete this.modulePermissions[this.moduleData.code];
                  } else {
                    this.modulePermissions[this.moduleData.code].list.splice(
                      i,
                      1
                    );
                  }
                }
              })
              .catch((error) => {
                console.log('Error getting cached document:', error);
              });
          }
        );
      }
    }
  }

  checkPermissions(params) {
    const module: Module = params.module;
    const loading = params.loading;

    if (
      !this.modulePermissions[module.code] ||
      Object.keys(this.modulePermissions[module.code]).length <= 0
    ) {
      this.modulePermissions[module.code] = this.role.permissions[module.code];

      this.role.permissions[module.code].list.forEach((modulePermission) => {
        modulePermission
          .get()
          .then((doc) => {
            const newPermission = doc.data();

            this.permissions.map((permission: Permission) => {
              if (!permission.checked) {
                if (newPermission.uuid === permission.uuid) {
                  permission.checked = true;
                } else {
                  permission.checked = false;
                }
              }
              loading.dismiss();
              return permission;
            });
          })
          .catch((error) => {
            console.log('Error getting cached document:', error);
          });
      });
    } else {
      if (
        this.modulePermissions[module.code] &&
        Object.keys(this.modulePermissions[module.code]).length > 0 &&
        this.modulePermissions[module.code].list &&
        this.modulePermissions[module.code].list.length > 0
      ) {
        this.modulePermissions[module.code].list.forEach((modulePermission) => {
          modulePermission
            .get()
            .then((doc) => {
              const newPermission: Permission = doc.data();

              this.permissions.map((permission: Permission) => {
                if (!permission.checked) {
                  if (newPermission.uuid === permission.uuid) {
                    permission.checked = true;
                  } else {
                    permission.checked = false;
                  }
                }

                loading.dismiss();

                return permission;
              });
            })
            .catch((error) => {
              console.log('Error getting cached document:', error);
            });
        });
      } else {
        loading.dismiss();
      }
    }
  }

  async selectModule(event, module: Module) {
    event.preventDefault();
    event.stopPropagation();

    const loading = await this.loadingController.create({
      message: 'Cargando...'
    });

    await loading.present();

    this.permissions.map((permission: Permission) => {
      permission.checked = false;

      return permission;
    });

    if (this.uuid) {
      if (
        Object.keys(this.role.permissions) &&
        Object.keys(this.role.permissions).length &&
        Object.keys(this.role.permissions).length > 0
      ) {
        Object.keys(this.role.permissions).forEach((value: string) => {
          if (
            this.role.permissions[value] &&
            this.role.permissions[value].module
          ) {
            this.role.permissions[value].module
              .get()
              .then((moduleResponse) => {
                if (moduleResponse.id === module.uuid) {
                  this.checkPermissions({ module, loading });
                } else {
                  loading.dismiss();
                }
              })
              .catch((error) => {
                console.log('Error getting cached document:', error);
              });
          }
        });
      } else {
        if (
          this.modulePermissions[module.code] &&
          Object.keys(this.modulePermissions[module.code]).length > 0 &&
          this.modulePermissions[module.code].list &&
          this.modulePermissions[module.code].list.length > 0
        ) {
          this.modulePermissions[module.code].list.forEach(
            (modulePermission) => {
              modulePermission
                .get()
                .then((doc) => {
                  const newPermission: Permission = doc.data();

                  this.permissions.map((permission: Permission) => {
                    if (!permission.checked) {
                      if (newPermission.uuid === permission.uuid) {
                        permission.checked = true;
                      } else {
                        permission.checked = false;
                      }
                    }

                    loading.dismiss();

                    return permission;
                  });
                })
                .catch((error) => {
                  console.log('Error getting cached document:', error);
                });
            }
          );
        } else {
          this.modulePermissions[module.code] = {};
          this.permissions.map((permission: Permission) => {
            delete permission.checked;
            return permission;
          });

          loading.dismiss();
        }
      }
    } else {
      if (
        !this.modulePermissions[module.code] ||
        Object.keys(this.modulePermissions[module.code]).length <= 0
      ) {
        this.modulePermissions[module.code] = {};
        this.permissions.map((permission: Permission) => {
          delete permission.checked;
          return permission;
        });
        loading.dismiss();
      } else {
        this.checkPermissions({ module, loading });
      }
    }
  }

  async loadRole() {
    const loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await loading.present();
    this.rolesService.getRole(this.uuid).subscribe(
      (success) => {
        loading.dismiss();
        this.role = success;
      },
      (fail) => {
        this.error = 'ocurrio un error en la base de datos';
        alert(fail);
      }
    );
  }

  async save() {
    this.role.permissions = this.modulePermissions;
    const loading = await this.loadingController.create({
      message: 'Guardando...'
    });
    await loading.present();
    if (this.uuid) {
      this.rolesService.updateRole(this.role, this.uuid).then(() => {
        loading.dismiss();
        this.navController.navigateForward('/web/roles');
      });
    } else {
      this.rolesService.createRole(this.role).then(() => {
        loading.dismiss();
        this.navController.navigateForward('/web/roles');
      });
    }
  }

  async delete() {
    const loading = await this.loadingController.create({
      message: 'Eliminando...'
    });
    await loading.present();
    if (this.uuid) {
      this.rolesService.deleteRole(this.uuid).then(() => {
        loading.dismiss();
        this.navController.navigateForward('/web/roles');
      });
    }
  }

  async loadModules() {
    const loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await loading.present();
    this.modules = this.modulesService.getModules();
    this.modules.subscribe(
      (success) => {
        loading.dismiss();
      },
      (fail) => {
        this.error = 'ocurrio un error en la base de datos';
        alert(fail);
      }
    );
  }

  async loadPermissions() {
    const loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    await loading.present();
    this.permissionsService.getPermissions().subscribe(
      (permissions: Permission[]) => {
        this.permissions = permissions;

        loading.dismiss();
      },
      (fail) => {
        this.error = 'ocurrio un error en la base de datos';
        alert(fail);
      }
    );
  }
}
