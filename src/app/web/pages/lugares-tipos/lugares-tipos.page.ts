import { Component, OnInit } from '@angular/core';

//Servicio de crud lugars
import { LugarTipoService } from 'src/app/services/lugares-tipos/lugar-tipo.service';

//Modelo lugars
import { LugarTipo } from 'src/app/models/lugares-tipos';

@Component({
  selector: 'app-lugares-tipos',
  templateUrl: './lugares-tipos.page.html',
  styleUrls: ['./lugares-tipos.page.scss'],
})
export class LugaresTiposPage implements OnInit {

  lugaresTipos: LugarTipo[];
  error: string;

  constructor(private lugarTipoService: LugarTipoService) { }

  ngOnInit() {
    this.lugarTipoService.obtenerLugaresTipos().subscribe(
      success => {
        this.lugaresTipos = success;
      }, fail => {
        this.error = "ocurrio un error en la base de datos";
        alert(fail);
      }
    );
  }
}
