import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular'

//Servicio de crud tipos lugares
import { LugarTipoService } from 'src/app/services/lugares-tipos/lugar-tipo.service';

//Modelo tipos lugares
import { LugarTipo } from 'src/app/models/lugares-tipos';

@Component({
  selector: 'app-acciones',
  templateUrl: './acciones.page.html',
  styleUrls: ['./acciones.page.scss'],
})
export class AccionesPage implements OnInit {

  public lugarTipo: LugarTipo = {
    estado: 1,
    nombre: "",
  };
  public id: string;
  public error: string;

  constructor(private activatedRoute: ActivatedRoute, private navController: NavController, private lugarTipoService: LugarTipoService, private loadingController: LoadingController) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.id) {
      this.cargarLugar();
    }
  }

  public async cargarLugar() {
    const cargando = await this.loadingController.create({
      message: "Cargando..."
    });
    await cargando.present();
    this.lugarTipoService.obtenerLugarTipo(this.id).subscribe(success => {
      cargando.dismiss();
      this.lugarTipo = success;
    }, fail => {
      this.error = "ocurrio un error en la base de datos";
      alert(fail);
    });
  }

  public async guardar() {
    const cargando = await this.loadingController.create({
      message: "Guardando..."
    });
    await cargando.present();
    if (this.id) {
      this.lugarTipoService.actualizarLugarTipo(this.lugarTipo, this.id).then(() => {
        cargando.dismiss();
        this.navController.navigateForward('/web/lugares');
      });
    } else {
      this.lugarTipoService.crearLugarTipo(this.lugarTipo).then(() => {
        cargando.dismiss();
        this.navController.navigateForward('/web/lugares/tipos');
      });
    }
  }

  public async eliminar() {
    const cargando = await this.loadingController.create({
      message: "Eliminando..."
    });
    await cargando.present();
    if (this.id) {
      this.lugarTipoService.eliminarLugarTipo(this.id).then(() => {
        cargando.dismiss();
        this.navController.navigateForward('/web/lugares/tipos');
      });
    }
  }
}
