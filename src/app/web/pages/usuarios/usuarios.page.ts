import { Component, OnInit } from '@angular/core';

//Servicio de crud usuarios
import { UsuarioService } from 'src/app/services/ususarios/usuario.service';

//Modelo usuarios
import { Usuario } from 'src/app/models/usuarios';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.page.html',
  styleUrls: ['./usuarios.page.scss'],
})
export class UsuariosPage implements OnInit {

  usuarios: Usuario[];
  error: string;

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.usuarioService.obtenerUsuarios().subscribe(success => {
      this.usuarios = success;
    }, fail => {
      this.error = "ocurrio un error en la base de datos";
      alert(fail);
    });
  }
}
