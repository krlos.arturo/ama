import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular'

//Servicio de crud usuarios
import { UsuarioService } from 'src/app/services/ususarios/usuario.service';

//Modelo usuarios
import { Usuario } from 'src/app/models/usuarios';

@Component({
  selector: 'app-acciones',
  templateUrl: './acciones.page.html',
  styleUrls: ['./acciones.page.scss'],
})
export class AccionesPage implements OnInit {

  public usuario: Usuario = {
    apellido: "",
    correo: "",
    direccion: "",
    estado: 1,
    imagen_perfil: "",
    nombre: "",
    usuario: "",
    comentarios:[],
    departamento:null,
    municipio:null,
    eps:null
  };
  public id: string;
  public error: string;

  constructor(private activatedRoute: ActivatedRoute, private navController: NavController, private usuarioService: UsuarioService, private loadingController: LoadingController) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.id) {
      this.cargarUsuario();
    }
  }

  public async cargarUsuario() {
    const cargando = await this.loadingController.create({
      message: "Cargando..."
    });
    await cargando.present();
    this.usuarioService.obtenerUsuario(this.id).subscribe(success => {
      cargando.dismiss();
      this.usuario = success;
    }, fail => {
      this.error = "ocurrio un error en la base de datos";
      alert(fail);
    });
  }

  public async eliminar() {
    const cargando = await this.loadingController.create({
      message: "Eliminando..."
    });
    await cargando.present();
    if (this.id) {
      this.usuarioService.eliminarUsuario(this.id).then(() => {
        cargando.dismiss();
        this.navController.navigateForward('/web/usuarios');
      });
    }
  }
}
