import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular'

//Servicio de crud lugares
import { LugarService } from 'src/app/services/lugares/lugar.service';
//Modelo lugares
import { Lugar } from 'src/app/models/lugares';
//Servicio de crud tipo lugares
import { LugarTipoService } from 'src/app/services/lugares-tipos/lugar-tipo.service';
//Modelo tipo lugares
import { LugarTipo } from 'src/app/models/lugares-tipos';

@Component({
  selector: 'app-acciones',
  templateUrl: './acciones.page.html',
  styleUrls: ['./acciones.page.scss'],
})
export class AccionesPage implements OnInit {

  public lugar: Lugar = {
    direccion: "",
    estado: 1,
    imagen_perfil: "",
    nombre: "",
    ubicacion: ""
  };

  tipoLugar: any = null;

  public lugaresTipos: LugarTipo[];
  public id: string;
  public error: string;

  constructor(private activatedRoute: ActivatedRoute, private navController: NavController, private lugarService: LugarService, private loadingController: LoadingController, private lugarTipoService: LugarTipoService) { }

  ngOnInit() {

    this.lugarTipoService.obtenerLugaresTipos().subscribe(
      success => {
        this.lugaresTipos = success;
      }, fail => {
        this.error = "ocurrio un error en la base de datos";
        alert(fail);
      }
    );
    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.id) {
      this.cargarLugar();
    }
  }

  public async cargarLugar() {
    const cargando = await this.loadingController.create({
      message: "Cargando..."
    });
    await cargando.present();
    this.lugarService.obtenerLugar(this.id).subscribe(success => {
      cargando.dismiss();
      this.lugar = success;
    }, fail => {
      this.error = "ocurrio un error en la base de datos";
      alert(fail);
    });
  }

  public async guardar() {

    const cargando = await this.loadingController.create({
      message: "Guardando..."
    });
    await cargando.present();
    if (this.id) {
      this.lugarService.actualizarLugar(this.lugar, this.id).then(() => {
        cargando.dismiss();
        this.navController.navigateForward('/web/lugares');
      });
    } else {
      this.lugarService.crearLugar(this.lugar).then(() => {
        cargando.dismiss();
        this.navController.navigateForward('/web/lugares');
      });
    }
  }

  public async eliminar() {
    const cargando = await this.loadingController.create({
      message: "Eliminando..."
    });
    await cargando.present();
    if (this.id) {
      this.lugarService.eliminarLugar(this.id).then(() => {
        cargando.dismiss();
        this.navController.navigateForward('/web/lugares');
      });
    }
  }
}
