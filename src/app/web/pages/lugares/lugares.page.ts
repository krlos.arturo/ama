import { Component, OnInit, ViewChild } from '@angular/core';

//Servicio de crud lugars
import { LugarService } from 'src/app/services/lugares/lugar.service';

//Modelo lugars
import { Lugar } from 'src/app/models/lugares';

//import { DatatableComponent } from '@swimlane/ngx-datatable/lib/components/datatable.component';

@Component({
  selector: 'app-lugares',
  templateUrl: './lugares.page.html',
  styleUrls: ['./lugares.page.scss'],
})
export class LugaresPage implements OnInit {

  public lugares: Lugar[];
  public error: string;
  public columns : any;
  temp = [];
  //@ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private lugarService: LugarService) {
    this.columns = [
      { prop: 'name' },
      { name: 'Summary' },
      { name: 'Company' }
    ];
  }

  ngOnInit() {
    this.lugarService.obtenerLugares().subscribe(
      success => {
        this.temp = [...success];
        this.lugares = success;
      }, fail => {
        this.error = "ocurrio un error en la base de datos";
        alert(fail);
      }
    );
  }

  public updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.lugares = temp;
    // Whenever the filter changes, always go back to the first page
    //this.table.offset = 0;
  }
}
