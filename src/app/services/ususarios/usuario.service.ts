import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from "angularfire2/firestore";
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

//Modelo usuarios
import { Usuario } from "src/app/models/usuarios";
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController, ModalController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private usuariosCollection: AngularFirestoreCollection<Usuario>;
  private usuarios: Observable<Usuario[]>;
  municipio
  departamento
  currentUser: string;
  constructor(private db: AngularFirestore, private auth: AngularFireAuth,private alertCtrl:AlertController,private ModalController:ModalController) { 
  
  }

  public obtenerUsuarios() {
    this.usuariosCollection = this.db.collection<Usuario>('usuarios');
    return this.usuarios = this.usuariosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(usuario => {
          const data = usuario.payload.doc.data();
          const id = usuario.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  public obtenerUsuario(id: string) {
    return this.usuariosCollection.doc<Usuario>(id).valueChanges();
  }

  public obtenerUsuarioEnSesion(){
    
    let usuariosCollection = this.db.collection<Usuario>('usuarios');
    let uid=this.auth.auth.currentUser.uid
  

    return usuariosCollection.doc<Usuario>(uid).valueChanges();


  }

  public actualizarUsuario(usuario: Usuario, id: string) {
    return this.usuariosCollection.doc(id).update(usuario);
  }

  public crearUsuario(id, usuario: Usuario) {
    this.usuariosCollection = this.db.collection<Usuario>('usuarios');
    return this.usuariosCollection.doc(id).set(usuario);
  }

  public eliminarUsuario(id: string) {
    return this.usuariosCollection.doc(id).delete();
  }
  public departamentoMunicipio(uid){
   
       this.db.collection('usuarios').doc(uid).snapshotChanges().subscribe(p=>{
         const data=p.payload.data() as Usuario
         if(data.departamento==null && data.municipio==null){
          this.presentPrompt(uid)
         }
       })
  
  }

  async presentPrompt(uid) {
 

   
  }

 
}
