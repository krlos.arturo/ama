import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// Modelo roles de usuarios
import { Role } from 'src/app/models/Role';

@Injectable({
  providedIn: 'root'
})
export class RolesService {
  private roleCollection: AngularFirestoreCollection<Role>;

  constructor(private db: AngularFirestore) {
    this.roleCollection = this.db.collection<Role>('roles');
  }

  public getRoles(): Observable<Role[]> {
    return this.roleCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((role) => {
          const data = role.payload.doc.data();
          const uuid = role.payload.doc.id;
          return { uuid, ...data };
        });
      })
    );
  }

  public getRole(uuid: string): Observable<Role> {
    return this.roleCollection.doc<Role>(uuid).valueChanges();
  }

  public updateRole(role: Role, uuid: string) {
    return this.roleCollection.doc(uuid).update(role);
  }

  public createRole(role: Role) {
    return this.roleCollection.add(role);
  }

  public deleteRole(uuid: string) {
    return this.roleCollection.doc(uuid).delete();
  }
}
