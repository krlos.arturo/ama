import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

//Modelo roles de usuarios
import { Role } from 'src/app/models/Role';

@Injectable({
  providedIn: 'root'
})
export class UsuarioRolService {
  private roleCollection: AngularFirestoreCollection<Role>;

  constructor(private db: AngularFirestore) {}

  public getRoles(): Observable<Role[]> {
    return this.db
      .collection<Role>('roles')
      .snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map((usuarioRol) => {
            const data = usuarioRol.payload.doc.data();
            const id = usuarioRol.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }

  public obtenerUsuarioRol(id: string) {
    return this.roleCollection.doc<Role>(id).valueChanges();
  }

  public actualizarUsuarioRol(usuarioRol: Role, id: string) {
    return this.roleCollection.doc(id).update(usuarioRol);
  }

  public crearUsuarioRol(usuarioRol: Role) {
    return this.roleCollection.add(usuarioRol);
  }

  public eliminarUsuarioRol(id: string) {
    return this.roleCollection.doc(id).delete();
  }
}
