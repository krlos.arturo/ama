import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { AngularFirestore } from 'angularfire2/firestore';
import { firestore } from 'firebase';
import { Clinicas } from 'src/app/interfaces/clinicas.interface';
import { usuarios } from 'src/app/interfaces/usuarios.interface';
import { Usuario } from 'src/app/models/usuarios';

@Injectable({
  providedIn: 'root'
})
export class NotificacionesService {

  constructor(private onesig:OneSignal,private db:AngularFirestore) { }


disparoNotificacion(mensaje,uid_clinica){

  this.db.collection('clinicas').doc(uid_clinica).snapshotChanges().subscribe(l=>{
    const data=l.payload.data() as Clinicas
    data.favoritos.forEach(p=>{
      this.db.collection('usuarios').doc(p).snapshotChanges().subscribe(k=>{
        const coaggula=k.payload.data() as usuarios
        let obj:any={
             
          headings:{en:mensaje.clinica},
          contents:{en:mensaje.content},
          include_player_ids:[coaggula.oneSigID]
        }
  
        
        this.onesig.postNotification(obj).then(k=>{
          obj=null
          coaggula.oneSigID=null
          uid_clinica=null
          mensaje=null
          return null 
        })
    
      })
     })
  })
 

    
  }


}
