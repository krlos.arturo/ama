import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { NavController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { firestore } from 'firebase';
import {NetworkService} from '../../services/networks/network.service';
import swal from 'sweetalert'
import { Clinicas } from 'src/app/interfaces/clinicas.interface';
import { Usuario } from 'src/app/models/usuarios';
import { usuarios } from 'src/app/interfaces/usuarios.interface';
export interface ClaveComent{valor_id:number}
@Injectable({
  providedIn: 'root'
})
export class ComentarioService {
  uid;
  constructor(private db:AngularFirestore,private navController:NavController,private auth:AngularFireAuth,private toastController:ToastController,private network:NetworkService) {
    this.auth.auth.onAuthStateChanged(k=>{
      this.uid=k.uid
    })

   }



   async registrarComentario(uid_clinica,estado,texto){

    this.db.collection('clave_comentario').doc('0nIFaJWbzrrdDy6f0Y4j').valueChanges().subscribe(l=>{
      console.log(l['valor_id'])
      var codigoComentario=Math.random().toString(36).substring(2)+'_'+this.uid
        var fecha=new Date()
        var mes=fecha.getMonth()+1
        var fecha_compuesta=fecha.getDate()+'/'+mes+'/'+fecha.getFullYear()
        
        //estados
        var recomendacion=0
        var sinservicio=0
        var congestion=0
        //
        var objeto={
          id:l['valor_id'],
          uid:this.uid,
          estado:estado,
          texto:texto,
          fecha:fecha_compuesta,
          codigoComentario:codigoComentario
        }
        if(estado==1){
          recomendacion=1
        }else if(estado==2){
          congestion=1
        }else{
          sinservicio=1
        }
     
       
         
          
          
          this.db.collection('clinicas').doc(uid_clinica).update({
        
           codigos_comentarios:firestore.FieldValue.arrayUnion(objeto),
           recomendaciones:firestore.FieldValue.increment(recomendacion),
           congestiones:firestore.FieldValue.increment(congestion),
           sinservicios:firestore.FieldValue.increment(sinservicio)
          
         }).then(()=>{
             this.navController.navigateBack(['/comentarios/',uid_clinica])
             this.toast()
             return null
         }).catch(error=>{
           console.log(error)
             swal("Houston! tenemos problemas..",error,"warning")
             this.network.verificarConexion()
         })
   })

     
   }


   async  resetearEstados(clinica){
    var fecha=new Date()
    var mes=fecha.getMonth()+1
    var fecha_compuesta=fecha.getDate()+'/'+mes+'/'+fecha.getFullYear()
    
     this.db.collection('clinicas').doc(clinica).snapshotChanges().subscribe(l=>{
       const data=l.payload.data() as Clinicas
       data.codigos_comentarios.forEach(p=>{
        console.log(p['fecha'])
         if(p['fecha']!=fecha_compuesta){
           console.log(p['fecha'])
           console.log(fecha_compuesta)
            l.payload.ref.update({
              codigos_comentarios:[],
              recomendaciones:0,
              congestiones:0,
              sinservicios:0,
              actividad:'disponible'

            })
         }
       })
       
          
     })
   }

   async toast(){
      const data=await this.toastController.create({
        message:"Comentario Registrado",
        color:'primary',
        position:'top',
        duration:2000
      })
      data.present()
   }


   
}
