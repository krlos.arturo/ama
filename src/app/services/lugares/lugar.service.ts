import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from "angularfire2/firestore";
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

//Modelo lugars
import { Lugar } from "src/app/models/lugares";

@Injectable({
  providedIn: 'root'
})
export class LugarService {

  private lugaresCollection: AngularFirestoreCollection<Lugar>;
  private lugares: Observable<Lugar[]>;

  constructor(private db: AngularFirestore) {

  }

  public obtenerLugares() {
    this.lugaresCollection = this.db.collection<Lugar>('lugares');
    return this.lugares = this.lugaresCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(lugar => {
          const data = lugar.payload.doc.data();
          const id = lugar.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  public obtenerLugar(id: string) {
    return this.lugaresCollection.doc<Lugar>(id).valueChanges();
  }

  public actualizarLugar(lugar: Lugar, id: string) {
    return this.lugaresCollection.doc(id).update(lugar);
  }

  public crearLugar(lugar: Lugar) {
    return this.lugaresCollection.add(lugar);
  }

  public eliminarLugar(id: string) {
    return this.lugaresCollection.doc(id).delete();
  }
}
