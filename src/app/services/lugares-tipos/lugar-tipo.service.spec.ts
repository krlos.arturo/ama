import { TestBed } from '@angular/core/testing';

import { LugarTipoService } from './lugar-tipo.service';

describe('LugarTipoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LugarTipoService = TestBed.get(LugarTipoService);
    expect(service).toBeTruthy();
  });
});
