import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from "angularfire2/firestore";
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

//Modelo tipo lugares
import { LugarTipo } from "src/app/models/lugares-tipos";

@Injectable({
  providedIn: 'root'
})
export class LugarTipoService {

  private lugaresTiposCollection: AngularFirestoreCollection<LugarTipo>;
  private lugaresTipos: Observable<LugarTipo[]>;

  constructor(private db: AngularFirestore) { }

  public obtenerLugaresTipos() {
    this.lugaresTiposCollection = this.db.collection<LugarTipo>('lugares_tipos');
    return this.lugaresTipos = this.lugaresTiposCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(lugarTipo => {
          const data = lugarTipo.payload.doc.data();
          const id = lugarTipo.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  obtenerReferenciaTipoLugar(id: string){
    return this.lugaresTiposCollection.doc<LugarTipo>(id);
  }

  public obtenerLugarTipo(id: string) {
    return this.lugaresTiposCollection.doc<LugarTipo>(id).valueChanges();
  }

  public actualizarLugarTipo(lugarTipo: LugarTipo, id: string) {
    return this.lugaresTiposCollection.doc(id).update(lugarTipo);
  }

  public crearLugarTipo(lugarTipo: LugarTipo) {
    return this.lugaresTiposCollection.add(lugarTipo);
  }

  public eliminarLugarTipo(id: string) {
    return this.lugaresTiposCollection.doc(id).delete();
  }
}
