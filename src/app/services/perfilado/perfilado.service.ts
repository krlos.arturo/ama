import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { ToastController } from '@ionic/angular';
import { FirebaseStorage } from 'angularfire2';

import { finalize } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class PerfiladoService {
  id;
  uploadPercent: any;
  base64Str;
  smallImage: any;
  constructor(private camera:Camera,private toastcontroller:ToastController,private auth:AngularFireAuth,private db:AngularFirestore) { 
    this.auth.auth.onAuthStateChanged(k=>{
      this.id=k.uid
    })
  }




  async toastController(){
   const presenta=await this.toastcontroller.create({
      message:"Foto Cargada",
      duration:2000
    })
    presenta.present()
  }
}
