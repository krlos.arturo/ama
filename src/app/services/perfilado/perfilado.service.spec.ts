import { TestBed } from '@angular/core/testing';

import { PerfiladoService } from './perfilado.service';

describe('PerfiladoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PerfiladoService = TestBed.get(PerfiladoService);
    expect(service).toBeTruthy();
  });
});
