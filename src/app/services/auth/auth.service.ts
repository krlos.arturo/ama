import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth' 
import { auth } from 'firebase/app';
import { Platform, AlertController } from '@ionic/angular';
import { Usuario } from 'src/app/models/usuarios';
import { UsuarioService } from '../ususarios/usuario.service';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { Router } from '@angular/router';
import { estados } from 'src/app/Enums/Estados';
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { webClientId } from 'src/util/FirebaseConfig';
import { reject } from 'q';
import { restablecimientoPassErrorMessages } from 'src/app/Enums/ErroresFirebase';
import { AngularFirestore } from 'angularfire2/firestore';
import { usuarios } from 'src/app/interfaces/usuarios.interface';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  errorRestablecerPass = restablecimientoPassErrorMessages;

  constructor( 
    private fireAuth: AngularFireAuth,
    private google: GooglePlus, 
    private usuarioService: UsuarioService,
    private platform: Platform, 
    private ngZone: NgZone,
    private router: Router,
    private alert: AlertController,
    private fireStore:AngularFirestore
  ) {

   }
  obtenerTokenSession(){
    return this.fireAuth.auth
  }
  //login generico con correo
  login(email: string, password: string){

    return new Promise((resolve, reject) =>{
      this.fireAuth.auth.signInWithEmailAndPassword(email, password).then(
        success=> {
          this.fireStore.collection('usuarios').doc(success.user.uid).snapshotChanges().subscribe(k=>{
            const role=k.payload.data() as usuarios
            if(role.estado==1){
              this.ngZone.run(()=>{
                this.router.navigateByUrl('/app/home');
                resolve(true);
              });
            }else{
              this.ngZone.run(()=>{
                this.router.navigateByUrl('/web/home');
                resolve(true);
              });
            }
          })
         
        }
      ).catch(error => reject(error));
    });

  }

  registrarConEmail(usuario: Usuario, contrasena: string){

    return new Promise((resolve, reject)=>{

      this.fireAuth.auth.createUserWithEmailAndPassword(usuario.correo, contrasena).then(
        credencialesDeUsuario => {
          this.usuarioService.crearUsuario(credencialesDeUsuario.user.uid, usuario).then(
            () =>{
              let respuesta = {code: null};
              if(credencialesDeUsuario.user.emailVerified){
                this.ngZone.run(()=>{
                  this.router.navigateByUrl('/app/home')
                });
                respuesta.code = "valid";
              }else{
                credencialesDeUsuario.user.sendEmailVerification();
                this.informarSobreVerificacion();
                respuesta.code = "verify";
              }
              resolve(respuesta);
            }
          ).catch(
            error => {
              reject(error);
              console.log(error);
            }
          )
        } 
      ).catch(
        error => {
          reject(error);
          console.log(error)
        }
      )
    });
  } 

  logWithFacebook(){
    if (this.platform.is('cordova')){
      //this.facebookLoginNative();
    }else{
      this.facebookLoginWeb();
    }
  }
  
  async facebookLoginWeb(){
    const provider = new auth.FacebookAuthProvider()
    const credential = await this.fireAuth.auth.signInWithPopup(provider);

    console.log(credential);
    

  }

  logWithGoogle(){

    if (this.platform.is('cordova')){
      this.googleLoginNative()
    }else{
      this.googleLoginWeb()
    }
  }

  async googleLoginWeb(){
    const provider = new auth.GoogleAuthProvider()
    const credential = await this.fireAuth.auth.signInWithPopup(provider)
    
    if(credential.additionalUserInfo.isNewUser){

      let usuario : Usuario= {
        apellido: null,
        correo: credential.user.email,
        direccion: null,
        estado: estados.ACTIVO,
        imagen_perfil: credential.user.photoURL,
        nombre: credential.user.displayName,
        usuario: null,
        comentarios:[],
        departamento:'',
          municipio:'',
          eps:null
      }

      this.usuarioService.crearUsuario(credential.user.uid, usuario).then(() => {
        this.ngZone.run(()=>{
          this.router.navigateByUrl('/app/home')
        });
      }).catch(error => {
        console.log(error)
      });

    }else{
      
      this.ngZone.run(()=>{
        this.router.navigateByUrl('/app/home');
      })
    
    }
  }

  async googleLoginNative(){
    
    const usuario = await this.google.login({
      'webClientId': webClientId,
      'offline': true,
      'scopes': 'profile email'
    })

    let credential = auth.GoogleAuthProvider.credential(usuario.idToken);
    
    this.fireAuth.auth.signInWithCredential(
      credential
    ).then(
      credencialesDeUsuario => {
        
        let user : Usuario= {
          apellido: null,
          correo: credencialesDeUsuario.user.email,
          direccion: null,
          estado: estados.ACTIVO,
          imagen_perfil: credencialesDeUsuario.user.photoURL,
          nombre: credencialesDeUsuario.user.displayName,
          usuario: null,
          departamento:'',
          municipio:'',
          comentarios:[],
          eps:null
        }
        
        this.usuarioService.crearUsuario(credencialesDeUsuario.user.uid, user).then(
          () =>{
            this.ngZone.run(()=>{
              this.router.navigateByUrl('/app/home')
            });
          }
        ).catch(
          error => {
            console.log(error)
          }
        )
      } 
    ).catch(
      error => {
        console.log(error)
      }
    )
  }

  public logout(){
    this.fireAuth.auth.signOut().then(() =>{
      this.ngZone.run(()=>{
        this.router.navigateByUrl('/app/login');
      });
    })
  }

  async restablecerContraseña(){
    const alert = await this.alert.create({
      header: 'Vale, restablezcamos tu contraseña',
      message: "Por favor ingresa el correo con que te registraste en AMA",
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'E-mail'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'boton-cerrar'
        }, 
        {
          text: 'Ok',
          handler: (data) => {
            this.solicitarContraseña(data.email);
          }
        }
      ]
    });

    await alert.present();
  }

  async solicitarContraseña(email){
    this.fireAuth.auth.sendPasswordResetEmail(email)
    .then(async () => {
      let alert = await this.alert.create({
        header: 'Todo listo!',
        message: `Te hemos enviado un correo a ${email} para que restablezcas tu contraseña con toda seguridad!`,
        buttons: [{
          text: 'Gracias'
        }]
      });
      alert.present();
    })
    .catch(async (error)=>{
      let alert = await this.alert.create({
        header: 'Ha ocurrido un error',
        message: this.errorRestablecerPass[error.code],
        buttons: [{
          text: 'Cerrar',
          role: 'cancel',
          cssClass: 'boton-cerrar'
        }]
      });
      alert.present();
    });
  }

  async informarSobreVerificacion(){
    const confirmar = await this.alert.create({
      header: 'Registro Completo!',
      subHeader: 'Solicitamos verificación',
      message: 'Hemos Enviado un correo a tú email, Por favor, verifica tu correo para activar tu cuenta.',
      buttons: ['OK']
    });
    await confirmar.present();
  }

  async reconocimientoRolPerfil(){
    this.fireAuth.auth.onAuthStateChanged(l=>{
        this.fireStore.collection('usuarios').doc(l.uid).snapshotChanges().subscribe(o=>{
            const data=o.payload.data() as usuarios
            if(data.estado==1){
            
                this.router.navigateByUrl('/app/home');
              
            }else{
              this.router.navigateByUrl('/web/home');
            }
        })
    })
  }

  async comprobarRegionalidad(){
    
  }
  
 

}
