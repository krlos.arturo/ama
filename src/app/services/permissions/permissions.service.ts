import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Permission } from 'src/app/models/Permission';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {
  private permissionCollection: AngularFirestoreCollection<Permission>;

  constructor(private db: AngularFirestore) {}

  public getPermissions(): Observable<Permission[]> {
    this.permissionCollection = this.db.collection<Permission>('permissions');
    return this.permissionCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((permission) => {
          const data = permission.payload.doc.data();
          const uuid = permission.payload.doc.id;
          return { uuid, ...data, doc: permission.payload.doc.ref };
        });
      })
    );
  }

  public getPermission(uuid: string): Observable<Permission> {
    return this.permissionCollection.doc<Permission>(uuid).valueChanges();
  }

  public updatePermission(permission: Permission, uuid: string) {
    return this.permissionCollection.doc(uuid).update(permission);
  }

  public createPermission(permission: Permission) {
    return this.permissionCollection.add(permission);
  }

  public deletePermission(uuid: string) {
    return this.permissionCollection.doc(uuid).delete();
  }
}
