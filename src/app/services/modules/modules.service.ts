import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Module } from 'src/app/models/Module';

@Injectable({
  providedIn: 'root'
})
export class ModulesService {
  private moduleCollection: AngularFirestoreCollection<Module>;

  constructor(private db: AngularFirestore) {}

  public getModules(): Observable<Module[]> {
    this.moduleCollection = this.db.collection<Module>('modules');
    return this.moduleCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((module) => {
          const data = module.payload.doc.data();
          const uuid = module.payload.doc.id;
          return { uuid, ...data, doc: module.payload.doc.ref };
        });
      })
    );
  }

  public getModule(uuid: string): Observable<Module> {
    return this.moduleCollection.doc<Module>(uuid).valueChanges();
  }

  public updateModule(module: Module, uuid: string) {
    return this.moduleCollection.doc(uuid).update(module);
  }

  public createModule(module: Module) {
    return this.moduleCollection.add(module);
  }

  public deleteModule(uuid: string) {
    return this.moduleCollection.doc(uuid).delete();
  }
}
