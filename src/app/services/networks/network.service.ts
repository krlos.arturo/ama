import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import swal from 'sweetalert'
@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(private network:Network) { }

  async verificarConexion(){
    let desconectado=this.network.onDisconnect().subscribe(()=>{
       swal("Ohhh...","Tu conexion a Internet no funciona bien","error")
    })

    setTimeout(() => {
      if (this.network.type === 'wifi') {
        console.log('we got a wifi connection, woohoo!');
      }
    }, 3000);
  }
}
