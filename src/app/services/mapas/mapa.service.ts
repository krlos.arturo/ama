import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { HttpClient } from '@angular/common/http';
import { Clinicas } from 'src/app/interfaces/clinicas.interface';
import { firestore } from 'firebase';
import { Favoritos } from 'src/app/interfaces/favoritos.interface';
import swal from 'sweetalert';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NetworkService } from '../networks/network.service';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';

@Injectable({
  providedIn: 'root'
})
export class MapaService {
   clinicas:any[]=[];
   sesion
  latitud: number;
  longitud: number;
  origen: { lat: number; lng: number; };
  constructor(private network:NetworkService,private launchNavigator:LaunchNavigator,private http:HttpClient,private store:AngularFirestore,private auth:AngularFireAuth,private geo:Geolocation) { 
   this.auth.auth.onAuthStateChanged(k=>{
     this.sesion=k.uid
   })
    this.geoposition()
  }

  async consultaInterinaUrgente(){
    return this.http.get("https://www.datos.gov.co/resource/xdk5-pm3f.json?c_digo_dane_del_departamento="+'73'+'&municipio='+'Ibagué')
  }

  
  async devolverClinicas(){
    return this.clinicas
  }

  async procesoFavoritos(nit,uid,clini){
    console.log(this.sesion)
    console.log(clini)
   this.store.collection('clinicas').doc(clini).update({
     usuariosFavoritos:firestore.FieldValue.arrayUnion(this.sesion)

   })
  }

verificarEstadoVerificado(nit,uid):number{
  var numero:number=0;
    this.store.collection('clinicas').snapshotChanges().subscribe(o=>{
      o.map(l=>{
        const data=l.payload.doc.data() as Clinicas
        if(data.favoritos.includes(nit)){
          numero=1;
      }
      })
      
    })
    return numero;
  }

  geoposition(){
    this.geo.getCurrentPosition().then(o=>{
      this.latitud= o.coords.latitude
      this.longitud=o.coords.longitude
      console.log(this.latitud)
      console.log(this.longitud)
    
      this.origen={lat:this.latitud,lng:this.longitud}
    })
    
      
    
  }


  async quitarFavoritos(nit,clini){
   this.auth.auth.onAuthStateChanged(o=>{
    this.store.collection('clinicas').doc(clini).update({
      favoritos:firestore.FieldValue.arrayRemove(o.uid)
    }).then(()=>{

    this.store.collection('favoritos').doc(o.uid).update({
      clinica:firestore.FieldValue.arrayRemove(nit)

    }).then(()=>{


      swal('Clinica quitada','Ya no recibiras notificaciones','success')
    
    })

    }).catch(()=>{
    this.network.verificarConexion()
    })
   })
   
  
   
  }


  async AbrirMetodico(origen,clinica:string){
    /**
     * var destino=data.coordinates
var Ruta="https://maps.google.com/maps?daddr="+data.coordinates.lat+','+data.coordinates.lng+"&amp;ll="
console.log(Ruta)
window.open(Ruta);

     */
    console.log(clinica)
    console.log(origen)
    var finals=clinica.trim()
    this.store.collection('clinicas').doc(finals).snapshotChanges().subscribe(p=>{
       const data=p.payload.data() as Clinicas
       var destino=data.coordinates
       console.log(destino)
       this.launchNavigator.navigate([data.coordinates.lat,data.coordinates.lng], {
        start: this.origen.lat + "," + this.origen.lng
      });      
    })
 
  }
}
