import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AppPage } from './app.page';
import { LoggedGuard } from '../guards/logged/logged.guard';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import { MunicipioDepaPage } from '../pages/municipio-depa/municipio-depa.page';
import { SelecciondepartamentalPage } from '../pages/selecciondepartamental/selecciondepartamental.page';
import { SelecciondepartamentalPageModule } from '../pages/selecciondepartamental/selecciondepartamental.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule',
    canActivate: [LoggedGuard]
  },
  {
    path: 'login',
    loadChildren: './pages/login/login.module#LoginPageModule'
  },
  { 
    path: 'mapa', 
    loadChildren: './pages/mapa/mapa.module#MapaPageModule',
    canActivate: [LoggedGuard]
  },
  { 
    path: 'user-profile', 
    loadChildren: './pages/user-profile/user-profile.module#UserProfilePageModule',
    canActivate: [LoggedGuard]
  },
  { path: 'clinicas-favoritas', loadChildren: './pages/clinicas-favoritas/clinicas-favoritas.module#ClinicasFavoritasPageModule' }
];

@NgModule({
  entryComponents:[
   
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),

  ],
  declarations: [AppPage],
  
  providers:[Geolocation]
})
export class AppPageModule { }
