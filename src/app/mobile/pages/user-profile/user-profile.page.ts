import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuarios';
import { UsuarioService } from 'src/app/services/ususarios/usuario.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ActionSheetController, NavController } from '@ionic/angular';
import { PerfiladoService } from 'src/app/services/perfilado/perfilado.service';
import { Router } from '@angular/router';
import { Base64, Base64Original } from '@ionic-native/base64';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { Camera ,CameraOptions} from '@ionic-native/camera/ngx';
import swal from 'sweetalert'
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
  
})

export class UserProfilePage implements OnInit {

  public usuario: Usuario
  uploadPercent: any;
  base64Str: any;
  uid;
  id
  codigo=0;
  loader: number=0;
  editprofile=0
  //De actualizar
   nombre
   apellido
   eps
  guardante: any;


  //
  constructor(private router:NavController,
    private db:AngularFirestore,
    private camera:Camera,
    private fire:AngularFirestore ,
    private authFire:AngularFireAuth,
    private storage:AngularFireStorage,
    private auth: AuthService, private perfilado:PerfiladoService, 
    private usuarioService: UsuarioService,
    private navController:NavController,
    private actionSheetController:ActionSheetController) {
     this.authFire.auth.onAuthStateChanged(k=>{
       this.id=k.uid
     })
  }

  ngOnInit() {
    this.cargarPerfil();
  }

  cerrarSesion(){
    this.auth.logout()
  }

  cargarPerfil(){
    this.usuarioService.obtenerUsuarioEnSesion().subscribe(
      data =>{
        this.usuario = data;
        this.eps=data.eps
      },
      error => {
        console.error(error);
      }
    );
  }
  async presentProfileUser(){
    const actionSheet= await this.actionSheetController.create({
      header: 'Foto de perfil',
      buttons: [{
        text: 'Tomar de camara',
        icon: 'camera',
        handler: () => {
          this.tomarFotoPerfil()
        }
      }, 
      {
        text: 'Subir de Galeria',
        icon: 'image',
        handler: () => {
          this.subirGaleria()
        }
      }, 
      {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
     })
     await actionSheet.present();
    
  }
  infoEps(){
    this.router.navigateForward(['/perfil-eps'])
  }

  async tomarFotoPerfil(){
    const options:CameraOptions={
      quality:50,
      targetHeight:600,
      targetWidth:600,
      destinationType:this.camera.DestinationType.DATA_URL,
      encodingType : this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    const id=Math.random().toString(36).substring(2);
    const filePath=`noticies/profile_${id}`
 
 
    this.camera.getPicture(options).then((imageData)=>{
    this.base64Str='data:image/jpeg;base64,'+imageData;

 
    this.base64Str='data:image/jpeg;base64,'+imageData;
  
      
      this.loader=1
      const id=Math.random().toString(36).substring(2);
      
      const filePath =  `perfilamiento-ama/profile_`+this.id
      const ref = this.storage.ref(filePath);
      const task=this.storage.upload(filePath,this.getBlob(this.base64Str))
     
      
      this.uploadPercent = task.percentageChanges()
      task.snapshotChanges().pipe(finalize(()=>ref.getDownloadURL().subscribe(l=>{
        this.guardante=l
        this.db.collection('usuarios').doc(this.id).update({
          imagen_perfil:this.guardante
        
        }).then(()=>{
          this.loader=0
        })
      
      }))).subscribe()
     
     
  
   

     
        
    
     })

  
  }

  async subirGaleria(){
    const cameraOptions={
      quality : 75,
      destinationType : this.camera.DestinationType.DATA_URL,
      sourceType : this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit : true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 500,
      targetHeight: 500,
      saveToPhotoAlbum: false
  
    }

    
  
    const id=Math.random().toString(36).substring(2);
    const filePath=`noticies/profile_${id}`
  
  
    this.camera.getPicture(cameraOptions).then(imageData=>{
   
  
        this.base64Str='data:image/jpeg;base64,'+imageData;
        
        console.log(this.getBlob(this.base64Str))
        this.loader=1
        /*this.db.collection('usuarios').doc(this.id).update({
          imagen_perfil:this.base64Str
        
        }).then(()=>{
          this.loader=0
        })*/
  

      
          const filePath =  `perfilamiento-ama/profile_`+this.id
          const ref = this.storage.ref(filePath);
          const task=this.storage.upload(filePath,this.getBlob(this.base64Str))
         
          
          this.uploadPercent = task.percentageChanges()
          task.snapshotChanges().pipe(finalize(()=>ref.getDownloadURL().subscribe(l=>{
            this.guardante=l
          
            this.db.collection('usuarios').doc(this.id).update({
              imagen_perfil:this.guardante
            
            }).then(()=>{
              this.loader=0
            })
          }))).subscribe()
         
         
       
      
      
       
    })
  }


  getBlob(b64Data) {
    
    let contentType = "image/jpeg";

    let sliceSize = 512;

    b64Data = b64Data.replace(/^(.*base64,)/, "");

    let byteCharacters = atob(b64Data); //decode base64

    let byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
  
      let slice = byteCharacters.slice(offset, offset + sliceSize);

      let byteNumbers = new Array(slice.length);

      for (let i = 0; i < slice.length; i++) {
  
        byteNumbers[i] = slice.charCodeAt(i);
  
      }

      let byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
  
    }

    let blob = new Blob(byteArrays, { type: contentType });

    return blob;
  
  }
  habilitarPerfilamiento(){
    this.codigo=1
  }

  cancelarPerfilamiento(){
    this.codigo=0
    this.editprofile=0
  }
  editProfileUser(){
    this.editprofile=1
  }
  actualizarDatos(){
    this.db.collection('usuarios').doc(this.id).update({
      nombre:this.usuario.nombre,
      apellido:this.usuario.apellido,
      eps:this.eps
    }).then(()=>{
      swal("Datos Actualizados...","Tus datos han sido actualizados","success")
    })
  }

  back(){
    this.navController.pop()
  }

}
