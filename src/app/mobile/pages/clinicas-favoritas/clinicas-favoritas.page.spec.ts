import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicasFavoritasPage } from './clinicas-favoritas.page';

describe('ClinicasFavoritasPage', () => {
  let component: ClinicasFavoritasPage;
  let fixture: ComponentFixture<ClinicasFavoritasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicasFavoritasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicasFavoritasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
