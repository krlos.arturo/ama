import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { Clinicas } from 'src/app/interfaces/clinicas.interface';
declare var google;
@Component({
  selector: 'app-clinicas-favoritas',
  templateUrl: './clinicas-favoritas.page.html',
  styleUrls: ['./clinicas-favoritas.page.scss'],
})
export class ClinicasFavoritasPage implements OnInit {
  map;
  bounds: any;
  clinicaco: any;
  id: any;
  texto
  estado: any;
  fecha: any;

  constructor(private auth:AngularFireAuth,private db:AngularFirestore,private activatedRoute:ActivatedRoute,private navController:NavController) { 
    this.clinicaco=this.activatedRoute.snapshot.paramMap.get('clinicaco')
    this.id=this.activatedRoute.snapshot.paramMap.get('id')
    this.varComment()
  }

  ngOnInit() {

  
  }


  varComment(){
    this.db.collection('clinicas').doc(this.clinicaco).snapshotChanges().subscribe(l=>{
      const data=l.payload.data() as Clinicas
        for(let y in data.codigos_comentarios){
          if(this.id==data.codigos_comentarios[y]['codigoComentario']){
           this.texto=data.codigos_comentarios[y]['texto']
           this.estado=data.codigos_comentarios[y]['estado']
           this.fecha=data.codigos_comentarios[y]['fecha']
           console.log(this.estado)
          }
           
        }

    })

  }


  close(){
    this.navController.pop()
  }
}
