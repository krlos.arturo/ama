import { Component,OnInit, ViewChild,Input, ElementRef, Renderer} from '@angular/core';

import {Geolocation} from '@ionic-native/geolocation/ngx';
import * as Mapboxgl from 'mapbox-gl';
import { repeat } from 'rxjs/operators';
import * as $ from 'jquery'
import { AlertController, IonContent, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { Marcador } from 'src/app/interfaces/marcadores.interface';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { MapaService } from 'src/app/services/mapas/mapa.service';
import { Clinicas } from 'src/app/interfaces/clinicas.interface';
import { AngularFirestore } from 'angularfire2/firestore';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, RequestOptionsArgs } from '@angular/http';
import { HTTP } from '@ionic-native/http/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import * as MarkerClusterer from "@google/markerclusterer"
import {
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes
} from '@angular/animations';
import { ActionSheetController } from '@ionic/angular';
import { ComentariosPage } from '../comentarios/comentarios.page';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Favoritos } from 'src/app/interfaces/favoritos.interface';
import { NetworkService } from 'src/app/services/networks/network.service';
import { firestore } from 'firebase';
import swal from 'sweetalert';
import { AngularFireAuth } from 'angularfire2/auth';
import { ClinicasFavoritasComponent } from 'src/app/components/clinicas-favoritas/clinicas-favoritas.component';
import { Reacciones } from 'src/app/interfaces/reacciones.interface';
import { ComentarioService } from 'src/app/services/comentarios/comentario.service';
import { Usuario } from 'src/app/models/usuarios';

declare var google;
declare var MarkerClusterer;
@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
  animations:[
    trigger('insertarTarjeta', [
      state("void", style({
        opacity: '0',
        transform: 'translateY(10vh)'
      })),
      state('*', style({
        opacity: '1',
        transform: 'translateY(0)'
      })),
      transition('void => *', animate(
        '0.4s ease-in'
      ))
    ])
  ]


})

export class MapaPage implements OnInit {
 
  longitud:number;
  latitud:number;
  clinicas:any[]=[]
  map=null;
  marker;
  probada;
  origen
  pasante=0;
  pasante2=1
  fluctuante=1
  conclave=0
  styles=[
    {
      "featureType": "all",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },

    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "all",
        "stylers": [
            {
                "gamma": "1.00"
            }
        ]
    },
    
    
    
 
  
    {
      "featureType": "poi.medical",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "on"
          },
          {
              "color": "red"
          }
      ]
  },
  {
      "featureType": "poi.medical",
      "elementType": "geometry",
      "stylers": [
          {
              "visibility": "on"
          }
      ]
  },
  {
      "featureType": "poi.medical",
      "elementType": "geometry.stroke",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "poi.medical",
      "elementType": "labels",
      "stylers": [
          {
              "visibility": "on"
          },
          {
              "color": "black"
          },
          {
              "saturation": "63"
          }
      ]
  },
  {
      "featureType": "poi.medical",
      "elementType": "labels.text",
      "stylers": [
          {
              "visibility": "on"
          }
      ]
  },
  {
      "featureType": "poi.medical",
      "elementType": "labels.text.fill",
      "stylers": [
          {
              "visibility": "on"
          },
          {
              "color": "red"
          }
      ]
  },
  {
      "featureType": "poi.medical",
      "elementType": "labels.text.stroke",
      "stylers": [
          {
              "visibility": "on"
          },
          {
              "color": "black"
          }
      ]
  },
    
   
    
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#a1cbfd"
            },
            {
                "visibility": "on"
            }
        ]
    }
]
  
  //Paramaetro de prejecucion
  parametroEjecutante=1;
  directionsService= new google.maps.DirectionsService()
  //Calculo ruta optima
  directionsDislay

  clinicaPositionLat;
  clinicaPositionLong;

  marcador: Marcador[] = [];

  //DETALLES DE LA CLINICA ELEGIDA
  contenedor=0;
  nombres
  direccion
  telefonos
  destino: any;
  espacioVital=0;
  distancia: any=0;
  tiempo: any;
  espacioTiempo=0;
  espacioDetrato: number;
  rutante=0;
  nit: string;
  tokenSession;
  verificadorFavoritos: number=0;
  uidClini: string;
  uidClinix: string;
  activador: number=0;
  actividad: string;
  buttons=0;
  municipio: any;
  codigo: any;
  location=0;
  bounds: any;
  latFavorito: any;
  contenedorFav=null
  lngFavorito: any;
  biden=1;
  claves: Set<unknown>;
  rutaClini: string='../../../../assets/img/icon-assets/ama-ico-flat.png';
  marcador2: { icon: string; position: any; uid: string; title: string; }[];
  marker2: any;
  consultaGeneral: any;
  likes: any;
  dislikes: number;
  niveles: string;
  mensajito: string;
  conclave1: number=0;
  conclave2: number=0;
  conclave3: number=0;
  fluctuanteLD=0
  estado
  departamento: any;
  constructor(private geo:Geolocation,
    private HTTP:HTTP,
    private store:AngularFirestore,
    private mapService:MapaService,
    private alertCtrl:AlertController,
    private nativeGeocoder:NativeGeocoder,
    private http:HttpClient,
    private navController:NavController,
    private launchNavigator:LaunchNavigator,
    public actionSheetController: ActionSheetController,
    private modalController:ModalController,
    private route:Router,
    private authService:AuthService,
    private network:NetworkService,
    private platform:Platform,
    private toastController:ToastController,
    private auth:AngularFireAuth,
    private comentariosService:ComentarioService
   
  

    ) { 
      this.geoposition()

      this.network.verificarConexion()
      this.authService.obtenerTokenSession().onAuthStateChanged(k=>{
        this.tokenSession=k.uid

        this.servicioDepartamento()
      });
  
     
      
    $(document).ready(function () {
      $('#informacion').hide()
  })
 
  }
  @ViewChild(IonContent) content: IonContent;
  ngOnInit() {
   
   
    
  }

  geoposition(){
    this.geo.getCurrentPosition().then(o=>{
  
      this.contenedorFav=null
      this.latitud= o.coords.latitude
      this.longitud=o.coords.longitude
      console.log(this.latitud)
      console.log(this.longitud)
      this.geolocation(this.latitud,this.longitud)
      this.origen={lat:this.latitud,lng:this.longitud}
    })
    
     
     setTimeout(() => {
       if (this.content.scrollToBottom) {
         this.content.scrollToBottom();
       }
     }, 400)
  }
  //METODO QUE CREA LOS MARCADORES
  renderMarkes(){
  
    this.marcador.forEach(mak=>{
      this.crearMarcador(mak)
    })
  }
  renderMarkes2(){
  
    this.marcador2.forEach(mak=>{
      this.crearMarcador2(mak)
    })
  }
  //GEOLOCALIZACION A PARTIR DEL MOVIL
  geolocation(lat:number,long:number){
 
    //Geocoder
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
  };
    // create a new map by passing HTMLElement
    const mapEle: HTMLElement = document.getElementById('map');
    // create LatLng object
 
 
    const myLatLng = {lat:lat, lng:long};
    // create map
    this.map = new google.maps.Map(mapEle, {
      center:myLatLng,
      disableDefaultUI: true,
      draggable: true,
      
      zoom: 12
    });
/**/ 
      
this.bounds = new google.maps.LatLngBounds();

 /**/   
    google.maps.event.addListenerOnce(this.map, 'idle', () => {

      
     
      mapEle.classList.add('show-map');
    });





  this.obtenerClinicas() 
 

  
     
 



  }
//OBTENER CLINICAS DE ACUERDO A LA RELACION DEL MUNICIPIO Y DEPARTAMENTO POR GEOCODIFICAICON
  obtenerClinicas(){
    
    /*this.http.get("https://www.datos.gov.co/resource/xdk5-pm3f.json?c_digo_dane_del_departamento="+cod+'&municipio='+muni).subscribe(l=>{
      var cosa=l[0]   
  
     
     })
     console.log(this.clinicas)*/

     this.consultaClinicas()
  }

 //ASIGNACION DE INFO SEGÚN MARCADOR SELECCIONADO
  async asignacionInformacion(position:Marcador){
    console.log(position)
    this.store.collection('clinicas',ref=>ref.where('uid','==',position.uid)).snapshotChanges().subscribe(p=>{
      p.map(o=>{
        const datai=o.payload.doc.data() as Clinicas
        
 
          this.nombres=datai.nombres.toLocaleUpperCase()
          this.direccion=datai.direcciones 
          this.telefonos=datai.telefonos
          this.nit=datai.nities
          this.uidClini=datai.uid.trim()
          this.actividad=datai.actividad
          this.likes=datai.reaccionLikes.length
          this.dislikes=datai.reaccionDislikes.length
          this.niveles=datai.niveles
          this.resetearEstados()
           if(datai.reaccionLikes.includes(this.tokenSession)){
              this.fluctuante=0
              this.fluctuanteLD=3
              this.mensajito="Te gusta"
           }else{
             if(datai.reaccionLikes==null || !datai.reaccionLikes.includes(this.tokenSession)){
                this.fluctuante=1
                this.fluctuanteLD=3
                this.mensajito="No te gusta"
             }else{
              this.mensajito=""
             }
           }
           


           if(datai.reaccionDislikes.includes(this.tokenSession)){
                this.fluctuante=2
              
           }else{
            if(datai.reaccionLikes==null || !datai.reaccionLikes.includes(this.tokenSession)){
              this.fluctuante=1
           }
           }


           if(datai.reaccionLikes.length==0 || datai.reaccionDislikes.length==0){
              this.fluctuanteLD=0
           }
          this.destino={lat:datai.coordinates.lat,lng:datai.coordinates.lng}
         this.verificarEstadoVerificado(this.nit,this.tokenSession)
          this.contenedor=1
          this.contenedorFav=1
          this.aquiMeAtienden()
        

        if(datai.recomendaciones>datai.sinservicios && datai.recomendaciones>datai.congestiones &&(datai.recomendaciones>=10)){
     

            o.payload.doc.ref.update({
              actividad:"recomendada"
            })
      
      
       }
       if(datai.congestiones>datai.recomendaciones && datai.congestiones>datai.sinservicios &&(datai.congestiones>=10)){
       
        o.payload.doc.ref.update({
          actividad:"congestionada"
        })
         
      
       }
      
       if(datai.sinservicios>datai.recomendaciones && datai.sinservicios>datai.congestiones &&(datai.sinservicios>=10)){
        
        o.payload.doc.ref.update({
          actividad:"sinservicio"
        })
         
       
       }
      
      })
    })
  }
  //CALCULO DE LA RUTA ESTIMADA ENTRE 2 PUNTOS
  calcularRuta(){
    
    var way=this.directionsService.route({
     origin:this.origen,
     destination:this.destino,
     
     travelMode:google.maps.TravelMode.DRIVING,
    
   },(response,status)=>{
       if(status=== google.maps.DirectionsStatus.OK){
         this.directionsDislay= new google.maps.DirectionsRenderer({
           map:this.map,
          
        })
         this.rutante=1;
         this.directionsDislay.setDirections(response);
         var routes=response.routes[0]
         console.log(routes)
       }else{
          alert("Could not display directions due to:"+status)
       }
   })
   
  





 }

 //OBTENER POSICION DE ACUERDO AL MARCADOR SELECCIONADO
  obtenerPosicion(marker:Marcador):boolean{
    var booelan=false;
 
    if(marker.position.lat!=this.latitud && marker.position.lng!=this.longitud){
      return booelan
    }
    return booelan
   }
   //CONSULTAS Y ASOCIAR CLINICAS CON EL CDIGO DEL MUNICIPIO ../../../../assets/img/icon-assets/ama-ico-flat.png  ../../../../assets/img/icon-assets/user-ama.ico
 
   async consultaClinicas(){
    var header= new HttpHeaders()
    window.navigator.geolocation.getCurrentPosition(d=>{
      
      var url="https://maps.googleapis.com/maps/api/geocode/json?latlng="+d.coords.latitude+","+d.coords.longitude+"&key="+'AIzaSyDHGPRDA0v1MRnt03RKr_DWdi_7U-Fcty8'
      this.http.get(url).subscribe(j=>{
         for(let x of j['results']){
          for (let h of x['types']){
          if(h=='administrative_area_level_1'){
           this.departamento=x['address_components'][0].long_name
           console.log(this.departamento)
           var codigo=''
           if(this.departamento=='Tolima'){
              codigo='73'
           }else if(this.departamento=='Antioquia'){
              codigo='05'
           }else if(this.departamento=='Arauca'){
            codigo='81'

           }else if(this.departamento=='Atlántico'){
            codigo='08'

           }else if(this.departamento=='Bogotá'){
            codigo='11'

           }else if(this.departamento=='Bolívar'){
            codigo='13'

           }else if(this.departamento=='Boyacá'){
            codigo='15'

           }else if(this.departamento=='Caldas'){
            codigo='17'

           }else if(this.departamento=='Caquetá'){
            codigo='18'

           }else if(this.departamento=='Casanare'){
            codigo='85'

           }else if(this.departamento=='Cauca'){
            codigo='19'

           }else if(this.departamento=='Cesar'){
            codigo='20'

           }else if(this.departamento=='Chocó'){
            codigo='27'
           }else if(this.departamento=='Córdoba'){
            codigo='23'
           }else if(this.departamento=='Cundinamarca'){
            codigo='25'

           }else if(this.departamento=='Guainía'){
            codigo='94'

           }else if(this.departamento=='Guaviare'){
            codigo='95'
           }else if(this.departamento=='Huila'){
            codigo='41'
           }else if(this.departamento=='La Guajira'){
            codigo='44'

           }else if(this.departamento=='Magdalena'){
            codigo='47'
           }else if(this.departamento=='Meta'){
            codigo='50'

           }else if(this.departamento=='Nariño'){
            codigo='52'

           }else if(this.departamento=='Norte de Santander'){
            codigo='54'

           }else if(this.departamento=='Putumayo'){
            codigo='86'

           }else if(this.departamento=='Quindío'){
            codigo='63'
           }else if(this.departamento=='Risaralda'){
            codigo='66'

           }else if(this.departamento=='San Andrés y Providencia'){
            codigo='88'
           }else if(this.departamento=='Sucre'){
            codigo='70'

           }else{
            codigo='76'

           }

           this.consultaGeneral= this.store.collection('clinicas',ref=>ref.where('departamentoIPSs','==',codigo))
           this.consultaGeneral.snapshotChanges().subscribe(l=>{
             l.map(k=>{
               if(k.payload.doc.exists){
                const data=k.payload.doc.data() as Clinicas
                this.claves=new Set()
               
             
                this.marcador=[
                 
                  {
                  
                  icon:this.rutaClini,
                  position:data.coordinates,
                  uid:data.uid,
                  title:data.nombres
                }]
               
                if(this.claves.has(data.uid)){
               
                  console.log(this.claves)
                }else{
                  this.claves.add(this.marcador)
                 
                  this.marker= new google.maps.Marker({
                 position:{lat:this.origen.lat,lng:this.origen.lng},
                 map:this.map,
             
                 icon:'../../../../assets/img/icon-assets/user-ama.ico',
                 label:'',
                })
                this.marker.setMap(this.map)
                this.marker= new google.maps.Marker({
                 position:data.coordinates,
                 map:this.map,
                 icon:'../../../../assets/img/icon-assets/ama-ico-flat.png',
                 label:'',
                })
                this.marker.setMap(this.map)
                this.renderMarkes()
                
                }
       
               
               }
               
         
               
             })
           })
          }
          }
         }
        })


     
    })
  
   }
   //CREA MARCADORES 
  crearMarcador(maker:Marcador){
   

 
  console.log(maker)
     this.desplegarOpciones(this.map,maker,this.marker)

  }

  crearMarcador2(maker:Marcador){
   

 
    console.log(maker)
    this.desplegarOpciones2(this.map,maker,this.marker)

 }


//DESPLIEGE DE MARCO DE OPCIONES 
 desplegarOpciones(map:any,marker:Marcador,mymaker:any){
   this.buttons=1
  mymaker.addListener("click", e => {
    this.location=1;
   console.log(marker.position)
   this.asignacionInformacion(marker)
   
  });

 }

 desplegarOpciones2(map:any,marker:Marcador,mymaker:any){
  this.buttons=1
 mymaker.addListener("click", e => {
   this.location=1;
  console.log(marker.position)
  this.asignacionInformacion(marker)
  
 });

}
//FUNCION QUE PERMITE CALCULO DE DISTANCIA Y TIEMPO DE LLEGADA DESDE PUNTO DE ORIGEN
 async aquiMeAtienden(){
  var headersx = new HttpHeaders();
  headersx.append('Access-Control-Allow-Origin' , '*');
  headersx.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
  headersx.append('Accept','application/json');
  headersx.append('content-type','application/json');
  let headers = {
    'Content-Type': 'application/json'
};
this.espacioTiempo=1;
 this.HTTP.get('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='+this.origen.lat+','+this.origen.lng+'&destinations='+this.destino.lat+','+this.destino.lng+'&key=AIzaSyDpdxJabo9anY2K_UCDDFapjrV5a3RC7nM',{},headers).then(data=>{
    var datai=JSON.parse(data.data)
   
   this.espacioVital=1
   this.espacioDetrato=0
   var distancia=datai['rows'][0]['elements'][0]['distance']['value']
   this.distancia=Math.round(distancia/1000)
   this.tiempo=datai['rows'][0]['elements'][0]['duration']['text']
   console.log(this.tiempo)

  }) 

  
 
  
   
 }

 async cancelarOperaciones(){
   
     this.navController.navigateForward('/mapa')
   
 }


 detallesActivador(){
   this.espacioTiempo=1;
 }
 cerrarInfo(){
   this.contenedor=0;
   this.buttons=0;
   this.location=0
   this.contenedorFav=null

   console.log(this.contenedorFav)

 }
 //ABRIR EL APP DE WAZE 
async AbrirMetodico(origen,destino){
   console.log("DESTINO",destino)
  /*this.launchNavigator.navigate([destino.lat,destino.lng], {
    start: this.origen.lat + "," + this.origen.lng

    var Ruta="https://maps.google.com/maps?daddr="+destino.lat+','+destino.lng+"&amp;ll="
  console.log(Ruta)
  window.open(Ruta);  
  });*/
  this.launchNavigator.navigate([destino.lat,destino.lng], {
    start: this.origen.lat + "," + this.origen.lng

  
  });
}

//OPCIONES PARA EJECUTAR EN EL MARCO DE OPCIONES
async presentActionSheet() {
  var iconic="checkmark-circle"
  const actionSheet = await this.actionSheetController.create({
    header: 'Acciones',
    cssClass: 'my-custom-class',
    buttons: [{
      text: 'COMENTARIOS',
      role: 'destructive',
      icon: 'chatbubbles',
      handler: () => {
        this.verComentarios(this.uidClini)
      }
    },
    {
      text: 'REPORTAR  O VER ESTADO',
      icon: iconic,
      role: 'destructive',
      handler: () => {
        console.log('Cancel clicked');
      }
    },
   
    {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present(); 
}
async verComentarios(uidClini){

  this.route.navigate(['/comentarios/',uidClini])
}
//COMUNICACION CON EL SERVICIO PARA AGREGAR A FAVORITOS UNA CLINICA

async anadirFavoritos(nit,clini){
  console.log(this.tokenSession)

 this.store.collection('clinicas').doc(clini).update({
     favoritos:firestore.FieldValue.arrayUnion(this.tokenSession)
     
 }).then(()=>{
   this.store.collection('favoritos').doc(this.tokenSession).update({
       clinica:firestore.FieldValue.arrayUnion(nit),
       
   }).then(()=>{

    
     this.activador=1
       nit=null
       swal('Clinica Agregada','Recibiras notificaciones de estados de actividad del sitio','success')
       this.geoposition()
    
   }).catch(()=>{
    this.store.collection('favoritos').doc(this.tokenSession).set({
      clinica:firestore.FieldValue.arrayUnion(nit),
      uid:this.tokenSession
  }).then(()=>{
    this.activador=0
      nit=null
      swal('Clinica Agregada','Recibiras notificaciones de estados de actividad del sitio','success')
      this.geoposition()
      this.marcador2=null
      this.marker2=null
     
  })
   })

     clini=null
     return null
 }).catch(()=>{
  this.network.verificarConexion()
 })
}

//QUITAR DE FAVORITOS
async quitarFavoritos(nit,clini){
 
  console.log(this.tokenSession)
  console.log(clini)

 this.store.collection('clinicas').doc(clini).update({
     favoritos:firestore.FieldValue.arrayRemove(this.tokenSession)
 }).then(()=>{

   this.store.collection('favoritos').doc(this.tokenSession).update({
     clinica:firestore.FieldValue.arrayRemove(nit)

   }).then(()=>{
     this.activador=0
      nit=null
      swal('Clinica quitada','Ya no recibiras notificaciones','success')
      this.geoposition()
      this.marcador2=null
      this.marker2=null
      return null;
   })
  
     clini=null
     return null
 }).catch(()=>{
  this.network.verificarConexion()
 })
}


//MEOTODO PARA VERIFICAR SI LA CLINICA SELECCIONADA HA SIDO AGREGADA A FAVORITOS O MO
verificarEstadoVerificado(nit,uid){
  
    this.store.collection('clinicas').doc(this.uidClini).snapshotChanges().subscribe(o=>{
      const data=o.payload.data() as Clinicas
      if(data.favoritos.includes(this.tokenSession)){
          this.verificadorFavoritos=1;
      }else{
        this.verificadorFavoritos=0;
      }
    })
   
  }

//METODO PARA FILTAR LAS CLINICAS FAVORTIAS
async filtradoClinicasFavortias(){
  this.auth.auth.onAuthStateChanged(k=>{
 

      this.store.collection('favoritos').doc(k.uid).snapshotChanges().subscribe(m=>{
         if(m.payload.id==k.uid){
            const datai=m.payload.data() as Favoritos
            datai.clinica.forEach(l=>{
              
              this.consultaGeneral=this.store.collection('clinicas',ref=>ref.where('nities','==',l)).snapshotChanges().subscribe(p=>{
                 p.map(g=>{
                   const data=g.payload.doc.data() as Clinicas
                   console.log(data.nombres)
                  
                   var latLng={lat:data.coordinates.lat,lng:data.coordinates.lng}
                   this.consultaClinicasFavoritas(latLng)
                 })
              })
            })
           
         }else{
           swal("Humm..","Al parecer no dispones de clinicas faavoritas","warning")
         }
      })
  })





}


async consultaClinicasFavoritas(coodinates){
  this.conclave3=1
  this.store.collection('clinicas',ref=>ref.where('coordinates','==',coodinates)).snapshotChanges().subscribe(p=>{
    p.map(h=>{
      if(h.payload.doc.exists){
        const data=h.payload.doc.data() as Clinicas
        this.marcador=[
           
          {
          
          icon:this.rutaClini,
          position:data.coordinates,
          uid:data.uid,
          title:data.nombres
        }]
        var latLng={lat:data.coordinates.lat,lng:data.coordinates.lng}
        console.log(latLng)
       var conjunto= new Set()
       conjunto.add(latLng)
      

       if(conjunto.has(latLng)==true){
          
        this.marker= new google.maps.Marker({
          position:{lat:this.origen.lat,lng:this.origen.lng},
          map:this.map,
      
          icon:'../../../../assets/img/icon-assets/user-ama.ico',
          label:'',
         })
         this.marker.setMap(this.map)
         this.marker= new google.maps.Marker({
          position:latLng,
          map:this.map,

          icon:'../../../../assets/img/icon-assets/favoritos.png',
          label:'',
         })
         
         this.renderMarkes()
         return false;
       }

 

      }
    })
  })

 
}
async servicioDepartamento()
{

  this.auth.auth.onAuthStateChanged(k=>{
    this.store.collection('usuarios').doc(k.uid).get().subscribe(k=>{
 
      this.estado=k.data()['nombre']
      
     
   })
   
  })

  
  
  
}
async consultaClinicasPrivadas(){
  this.conclave=1
  this.store.collection('clinicas',ref=>ref.where('niveles','==','privado')).snapshotChanges().subscribe(p=>{
    p.map(h=>{
      if(h.payload.doc.exists){
        const data=h.payload.doc.data() as Clinicas
        this.marcador=[
           
          {
          
          icon:this.rutaClini,
          position:data.coordinates,
          uid:data.uid,
          title:data.nombres
        }]
        var latLng={lat:data.coordinates.lat,lng:data.coordinates.lng}
        console.log(latLng)
       var conjunto= new Set()
       conjunto.add(latLng)
      

       if(conjunto.has(latLng)==true){
          
        this.marker= new google.maps.Marker({
          position:{lat:this.origen.lat,lng:this.origen.lng},
          map:this.map,
      
          icon:'../../../../assets/img/icon-assets/user-ama.ico',
          label:'',
         })
         this.marker.setMap(this.map)
         this.marker= new google.maps.Marker({
          position:latLng,
          map:this.map,

          icon:'../../../../assets/img/icon-assets/clinica_privada.png',
          label:'',
         })
         
         this.renderMarkes()
         return false;
       }

 

      }
    })
  })
  
}



async consultaClinicasPublicas(){
  this.conclave1=1
  this.store.collection('clinicas',ref=>ref.where('niveles','==','subsidiado')).snapshotChanges().subscribe(p=>{
    p.map(h=>{
      if(h.payload.doc.exists){
        const data=h.payload.doc.data() as Clinicas
        this.marcador=[
           
          {
          
          icon:this.rutaClini,
          position:data.coordinates,
          uid:data.uid,
          title:data.nombres
        }]
        var latLng={lat:data.coordinates.lat,lng:data.coordinates.lng}
        console.log(latLng)
       var conjunto= new Set()
       conjunto.add(latLng)

       if(conjunto.has(latLng)==true){
          
        this.marker= new google.maps.Marker({
          position:{lat:this.origen.lat,lng:this.origen.lng},
          map:this.map,
      
          icon:'../../../../assets/img/icon-assets/user-ama.ico',
          label:'',
         })
         this.marker.setMap(this.map)
         this.marker= new google.maps.Marker({
          position:data.coordinates,
          map:this.map,
         
          icon:'../../../../assets/img/icon-assets/clinica_publica.png',
          label:'',
         })
         this.marker.setMap(this.map)
         this.renderMarkes()
         return false;
       }

 

      }
    })
  })
  
}


async consultaClinicasMixtas(){
  this.conclave2=1
  this.store.collection('clinicas',ref=>ref.where('niveles','==','mixta')).snapshotChanges().subscribe(p=>{
    p.map(h=>{
      if(h.payload.doc.exists){
        const data=h.payload.doc.data() as Clinicas
        this.marcador=[
           
          {
          
          icon:this.rutaClini,
          position:data.coordinates,
          uid:data.uid,
          title:data.nombres
        }]
        var latLng={lat:data.coordinates.lat,lng:data.coordinates.lng}
        console.log(latLng)
       var conjunto= new Set()
       conjunto.add(latLng)

       if(conjunto.has(latLng)==true){
          
        this.marker= new google.maps.Marker({
          position:{lat:this.origen.lat,lng:this.origen.lng},
          map:this.map,
      
          icon:'../../../../assets/img/icon-assets/user-ama.ico',
          label:'',
         })
         this.marker.setMap(this.map)
         this.marker= new google.maps.Marker({
          position:data.coordinates,
          map:this.map,
         
          icon:'../../../../assets/img/icon-assets/clinica_mixta.png',
          label:'',
         })
         this.marker.setMap(this.map)
         this.renderMarkes()
         return false;
       }

 

      }
    })
  })
}
async geolicalizanteMaestro(coodinates){

      //Geocoder
      let options: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
    };
      // create a new map by passing HTMLElement
      const mapEle: HTMLElement = document.getElementById('map');
      // create LatLng object
   
   
      const myLatLng =coodinates;
      // create map
      this.map = new google.maps.Map(mapEle, {
        center:myLatLng,
        disableDefaultUI: true,
        draggable: true,
        
        zoom: 12
      });
  /**/ 
        
  this.bounds = new google.maps.LatLngBounds();
  
   /**/   
      google.maps.event.addListenerOnce(this.map, 'idle', () => {
  
        
       
        mapEle.classList.add('show-map');
      });
}

cerrarFavortias(){
  this.pasante=0
  this.pasante2=1
  this.biden=1
  this.conclave=0
  this.conclave1=0
  this.conclave2=0
  this.conclave3=0
  this.marcador2=null
this.marker2=null
  this.geoposition()
}


arrancarFiltros(){
  
}

async reaccionMegusta(estados,clinica,estado){

  
    this.store.collection('clinicas').doc(clinica).update({
        reaccionLikes:firestore.FieldValue.arrayUnion(this.tokenSession)
      }).finally(()=>{
        this.geoposition()
        this.fluctuante=0
        this.contenedor=1
        this.contenedorFav=1
        
        clinica=null
        
      })

    

  
  

 

}

async reaccionNoMegusta(estados,clinica,estado){

      this.store.collection('clinicas').doc(clinica).update({
        reaccionDislikes:firestore.FieldValue.arrayUnion(this.tokenSession)
      }).finally(()=>{
        this.geoposition()
        this.fluctuante=2
        this.contenedor=1
        this.contenedorFav=1
        
        clinica=null
        
      })
 
}

async informante(mensaje){
  const dat=await this.toastController.create({
    message:"Ya haz registrado el,"+mensaje,
    position:'top',
    duration:2000
  })

 dat.present()
}

async filtros(){
  var iconic="checkmark-circle"
  const actionSheet = await this.actionSheetController.create({
    header: 'Filtrar',
   
    buttons: [{
  
      text: 'Clinicas privadas',
      role: 'destructive',

      handler: () => {
        this.consultaClinicasPrivadas()

      }
    },
    

    {
      cssClass: 'my-custom-class',
      text: 'Clinicas publicas ',
      role: 'destructive',
    
      handler: () => {
        this.consultaClinicasPublicas()
        this.conclave1=1
      }
    },
    
    {
      text: 'Clinicas mixtas',
      role: 'destructive',
     
      handler: () => {
        this.consultaClinicasMixtas()
        this.conclave2=1
      }
    },
    
   
    {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present(); 
}
close(){
  this.navController.navigateForward(['/app/home'])
}
resetearEstados(){
  this.comentariosService.resetearEstados(this.uidClini)
}

regresionPublicas(){

}
regresionClinicasMixtas(){

}
regresionClinicasFavortias(){

}
}




