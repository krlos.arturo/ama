import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController, NavController, ToastController } from '@ionic/angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { ComentarioService } from 'src/app/services/comentarios/comentario.service';
import { MapaService } from 'src/app/services/mapas/mapa.service';

@Component({
  selector: 'app-comentario',
  templateUrl: './comentario.page.html',
  styleUrls: ['./comentario.page.scss'],
})
export class ComentarioPage implements OnInit {

  valorEstado
  texto
  clinica
  constructor(private modalController:ModalController,private navController:NavController,private toastController:ToastController,private mapService:MapaService,private routeAactivated:ActivatedRoute,private db:AngularFirestore,private comentarioService:ComentarioService) {
    this.clinica=this.routeAactivated.snapshot.paramMap.get('clinica')
   }

  ngOnInit() {}
  async salir(){
    this.navController.navigateBack(['/comentarios/',this.clinica])
  }


  asignarValorEstado(valor){
    this.valorEstado=valor
    console.log(this.valorEstado)
  }
  async realizarComentario(){
    console.log(this.texto)
   if(this.texto!=null && this.valorEstado!=null){
    this.comentarioService.registrarComentario(this.clinica,this.valorEstado,this.texto).then(()=>{
      this.mapService.geoposition()
    })
   }else{
      if(this.texto==null){
        this.toast("Texto")
      }else{
        this.toast("Estado")
      }
   }
    
  }


  async toast(mensaje){
    const data=await this.toastController.create({
      message:"Falta"+''+mensaje,
      color:'danger',
      duration:2000
    })
  }
}
