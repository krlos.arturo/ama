import { Component, OnInit, ViewChild } from '@angular/core';
import { whenRendered } from '@angular/core/src/render3';
import { ActivatedRoute, Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { IonContent, ModalController, NavController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { map } from 'rxjs/operators';
import { ComentarioComponent } from 'src/app/components/comentarios/comentario/comentario.component';
import { DepartamentosComponent } from 'src/app/components/departamentos/departamentos.component';
import { Clinicas } from 'src/app/interfaces/clinicas.interface';
import { Comentario } from 'src/app/interfaces/comentarios.interface';
import swal from 'sweetalert';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/models/usuarios';
import { NotificacionesService } from 'src/app/services/notificaciones/notificaciones.service';
import { ComentarioService } from 'src/app/services/comentarios/comentario.service';
import { MapaService } from 'src/app/services/mapas/mapa.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { firestore } from 'firebase';
declare var google;
@Component({
  selector: 'app-comentarios',
  templateUrl: './comentarios.page.html',
  styleUrls: ['./comentarios.page.scss'],
})
export class ComentariosPage implements OnInit {
  seccion='recomendation';
  clinicaco
  longitud: any;
  latitud: any;
  comentarios=[];
  usuariosCollection:AngularFirestoreCollection<Usuario>;
  usuariosObserbavle:Observable<Usuario[]>;
  origen: { lat: any; lng: any; };
  objetoPerfilamiento: any[]=[]
  actividad: string;
  @ViewChild(IonContent) content: IonContent;
  favorito: any;
  uid: string;
  nitie: string;
  uidcli: string;
  constructor(private route:Router,
    private activatedRoute:ActivatedRoute, 
    private db:AngularFirestore ,
    private modalController:ModalController,
    private geo:Geolocation,
    private navController:NavController,
    private notificaciones:NotificacionesService,
    private comentariosService:ComentarioService,
    private mapService:MapaService,
    private auth:AngularFireAuth
    ) {
    this.clinicaco=this.activatedRoute.snapshot.paramMap.get('clinica-cod')
    this.comentariosService.resetearEstados(this.clinicaco)
    console.log(this.clinicaco)
    this.geo.getCurrentPosition().then(s=>{
      this.longitud=s.coords.longitude
      this.latitud=s.coords.latitude
      
      

      this.origen={lat:this.latitud,lng:this.longitud}
      
   
    })

   
   }

  ngOnInit() {
    this.taerComentarios()
  
  }
  public cambioSegmento(evento) {
    this.seccion = evento.detail.value;
  }

  async realizarComentario(){
    this.navController.navigateForward(['/comentario/',this.clinicaco])
  
  }   

public geolocalizante(){
  //posicion clinica
 this.db.collection("clinicas").doc(this.clinicaco).get().subscribe(o=>{
    const data=o.data() as Clinicas
    const puntoFijo= new google.maps.LatLng(data.coordinates.lat,data.coordinates.lng)
    let posActual= new google.maps.LatLng(this.origen.lat,this.origen.lng)
    var distancia = google.maps.geometry.spherical.computeDistanceBetween(puntoFijo,posActual);
    var rondeo=Math.round(distancia)
    console.log(rondeo)
     if(rondeo<1000) { // 1km
      this.realizarComentario() 
  
       swal("Estas a menos de 1km de la clinica correspondite","Se activara la funcion de hacer comentarios","success")
      
       
     }else{
    
       
       swal("OOPSS..No te encuentras en los limites de la clinica asociada","Se activara la funcion de hacer comentarios cuando estes a menos de 1km de la clinica","warning")
     }
   }
  
 )

}


async taerComentarios(){
  setTimeout(() => {
    if (this.content.scrollToBottom) {
      this.content.scrollToBottom();
    }
  }, 400)
  var contador=0
  var objeto={
    clinica:"",
    content:""
  }
 this.db.collection('clinicas').doc(this.clinicaco).snapshotChanges().subscribe(k=>{
   const data=k.payload.data() as Clinicas
   this.nitie=data.nities
   this.uidcli=data.uid
   this.auth.auth.onAuthStateChanged(k=>{
    if(data.favoritos.includes(k.uid)){
      this.favorito='favoritos'
      this.uid=k.uid
    }else{
      this.favorito='nofavorito'
      this.uid=k.uid
    }
   })
  
   this.comentarios=data.codigos_comentarios
   if(data.recomendaciones>data.sinservicios && data.recomendaciones>data.congestiones &&(data.recomendaciones>=10)){
     
    this.actividad="recomendada"
    


 }else if(data.congestiones>data.recomendaciones && data.congestiones>data.sinservicios &&(data.congestiones>=10)){
 
    this.actividad="congestionada"
    objeto.clinica=data.nombres
    objeto.content="Alerta, la clinica presenta estado de CONGESTION"
    this.notificaciones.disparoNotificacion(objeto,this.clinicaco)

 }else if(data.sinservicios>data.recomendaciones && data.sinservicios>data.congestiones &&(data.sinservicios>=10)){
  
    this.actividad="sinservicio"   
    objeto.clinica=data.nombres
    objeto.content="Alerta, la clinica presenta estado de SIN SERVICIO"
    this.notificaciones.disparoNotificacion(objeto,this.clinicaco)
    
 }else{
   this.actividad="disponible"
 }

   console.log(this.actividad)

   this.taerPerfiles()
 })
}

async taerPerfiles(){
  this.usuariosCollection= this.db.collection('usuarios');
  this.usuariosObserbavle =this.usuariosCollection.snapshotChanges().pipe(map(actions => {
   return actions.map(action => {
   const data = action.payload.doc.data() as Usuario;
   const id = action.payload.doc.id;
         
   return {id, ...data};
});
}));

   
 
}

async salir(){
  this.navController.navigateForward(['/comentarios/',this.clinicaco])
}
back(){
  this.navController.pop()
}

verComentarioCompleto(id,clinicaco){
  this.navController.navigateForward(['clinicas-favoritas',id,clinicaco])
}

quitarFavoritos(nit,clini:string){
  var finals=clini.trim()
console.log(clini) 
console.log(nit)
  this.auth.auth.onAuthStateChanged(o=>{
    this.db.collection('clinicas').doc(finals).update({
      favoritos:firestore.FieldValue.arrayRemove(o.uid)
    }).then(()=>{

    this.db.collection('favoritos').doc(o.uid).update({
      clinica:firestore.FieldValue.arrayRemove(nit)

    }).then(()=>{


      swal('Clinica quitada','Ya no recibiras notificaciones','success')
    
    })

    }).catch(()=>{
    
    })
   })
}
}
