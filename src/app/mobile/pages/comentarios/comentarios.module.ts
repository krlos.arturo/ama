import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ComentariosPage } from './comentarios.page';
import { ComponentModule } from 'src/app/components/component.module';
import { ComentarioComponent } from 'src/app/components/comentarios/comentario/comentario.component';
import { DepartamentosComponent } from 'src/app/components/departamentos/departamentos.component';

const routes: Routes = [
  {
    path: '',
    component: ComentariosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    RouterModule.forChild(routes)
  ],
  entryComponents:[ComentarioComponent,DepartamentosComponent],
  exports:[ComentarioComponent,DepartamentosComponent],
  declarations: [ComentariosPage]
})
export class ComentariosPageModule {}
