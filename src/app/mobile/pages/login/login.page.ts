import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms'
import { AuthService } from 'src/app/services/auth/auth.service';
import { Usuario } from 'src/app/models/usuarios';
import { estados } from 'src/app/Enums/Estados';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { loginErrorMessages, resgistroConEmailErrorMessages } from 'src/app/Enums/ErroresFirebase';
import { NetworkService } from 'src/app/services/networks/network.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public mensajesLogin = loginErrorMessages;
  public mensajesRegistro = resgistroConEmailErrorMessages;
 public loader=0;
  public seccion = "login";
  public loginForm: FormGroup;
  public loginValidationMessages: any;
  public registerForm: FormGroup;
  public registerValidationMessages: any;
  public departamentos=[];
  private contrasena = null;
  private confirmarContrasena = null;
  public codigoDane;
  public activadorMunicipios
  errorLogin = null;
  errorRegistro = null;

  public usuario: Usuario = {
    apellido: null,
    correo: null,
    direccion: null,
    estado: estados.ACTIVO,
    imagen_perfil: null,
    nombre: null,
    usuario: null,
    departamento:null,
    municipio:null,
    comentarios:[],
    eps:null
  };


  

  constructor(private formBuilder: FormBuilder, private authService: AuthService,private http:HttpClient,private network:NetworkService) {
    
    this.inicializarValidadores();
    this.inicializarMensajesValidadores();
   

  }

  ngOnInit() {
  }

  public cambioSegmento(evento) {
    this.seccion = evento.detail.value;
  }

  public logearORegistrar(){
    switch (this.seccion) {
      case "login":
        this.login();
        break;
    
      default:
        this.registrarPorEmail();
        break;
    }
  }

  private inicializarValidadores(){
    /* Validación de campos en formulario de login*/
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.email,
      ])),
      password: new FormControl('', Validators.required)
    });

    /** Validación de campos en formulario de registro*/
    this.registerForm = this.formBuilder.group({
      nombres: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z]+$")
      ])),
      apellidos: new FormControl('',Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z]+$")
      ])),
      email: new FormControl('',Validators.compose([
        Validators.email,
        Validators.required
      ])),
      
      password: new FormControl('', Validators.compose([
        Validators.required
      ])),
      confirmPassword: new FormControl('', Validators.compose([
        Validators.required
      ]))
    },{validator: LoginPage.passwordsCoinciden}
    );
  }

  inicializarMensajesValidadores(){

    this.loginValidationMessages = {
      email:[
        {
          type: "required",
          message: "Debes ingresar tu email"
        },
        {
          type: "email",
          message: "Debes escribir un correo valido"
        }
      ],
      password:[
        {
          type: "required",
          message: "Debes ingresar tu contraseña"
        }
      ]
    }

    this.registerValidationMessages = {
      nombres:[
        {
          type: "required",
          message: "Al menos tu primer nombre es requerido"
        },
        {
          type: "pattern",
          message: "tu nombre solo puede contener letras"
        }
      ],
      apellidos:[
        {
          type: "required",
          message: "Al menos tu primer apellido es requerido"
        },
        {
          type: "pattern",
          message: "tu apellido solo puede contener letras"
        }
      ],
      email:[
        {
          type: "required",
          message: "Debes ingresar tu email"
        },
        {
          type: "email",
          message: "Debes escribir un correo valido"
        }
      ],
      password:[
        {
          type: "required",
          message: "Debes ingresar tu contraseña"
        }
      ],
     
      confirmPassword:[
        {
          type: "required",
          message: "Debes confirmar tu contraseña"
        }
      ]
    }
  }

  public login(){
    this.loader=1
    let email = this.loginForm.get('email').value;
    let password = this.loginForm.get('password').value;
    this.authService.login(email, password).catch(
      error => {
        this.errorLogin = this.mensajesLogin[error.code];
      }
    ).then(()=>{
      this.loader=0
    })
  }

  public loginGoogle(){
    this.authService.logWithGoogle();
  }


  public loginFacebook(){
    this.authService.logWithFacebook();
  }


  public registrarPorEmail(){
    this.usuario.nombre = this.registerForm.get('nombres').value;
    this.usuario.apellido = this.registerForm.get('apellidos').value;
    this.usuario.correo = this.registerForm.get('email').value;
    this.usuario.imagen_perfil="../../../../assets/img/profile_user.png"
    this.contrasena = this.registerForm.get('password').value;
    this.authService.registrarConEmail(this.usuario, this.contrasena)
    .then( respuesta => {
      console.log(respuesta);
    })
    .catch( error => {
      this.errorRegistro = this.mensajesRegistro[error.code];
      this.network.verificarConexion()
    })
  }

  async solicitarPassword(){
    await this.authService.restablecerContraseña();
  }

  static passwordsCoinciden(grupo: FormGroup): {[err: string]: any}  {

    let password = grupo.get('password');
    let confirmPassword = grupo.get('confirmPassword');
    let respuesta: {[error: string]: any} = {};
    if ((password.touched || confirmPassword.touched) && password.value !== confirmPassword.value){
      respuesta['passwordNoCoincide'] = true;
    }
    return respuesta;
  }

  async obtenerDepartamentos(){

    var municipios
    var cod;
  
     
      cod=this.registerForm.get('departamento').value
      console.log(cod)
      if(cod.length!=0){
        this.activadorMunicipios=1
        this.http.get("https://www.datos.gov.co/resource/xdk5-pm3f.json?c_digo_dane_del_departamento="+cod).subscribe(k=>{
          this.departamentos.push(k)
          console.log(this.departamentos['0'])
         
        })
      }
    
    
   
  }

  asignarCodigo(){
   this.codigoDane= this.registerForm.get('departamento').value
   console.log(this.codigoDane)
  }

}