import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import { MunicipioDepaPage } from 'src/app/pages/municipio-depa/municipio-depa.page';
import { SelecciondepartamentalPageModule } from 'src/app/pages/selecciondepartamental/selecciondepartamental.module';
import { SelecciondepartamentalPage } from 'src/app/pages/selecciondepartamental/selecciondepartamental.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    
  ],

  declarations: [HomePage]
})
export class HomePageModule {}
