import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UsuarioService } from 'src/app/services/ususarios/usuario.service';
import { Usuario } from 'src/app/models/usuarios';
import { AngularFireAuth } from 'angularfire2/auth';
import { ModalController, AlertController, NavController, ActionSheetController } from '@ionic/angular';
import { MunicipioDepaPage } from 'src/app/pages/municipio-depa/municipio-depa.page';
import { SelecciondepartamentalPageModule } from 'src/app/pages/selecciondepartamental/selecciondepartamental.module';
import { SelecciondepartamentalPage } from 'src/app/pages/selecciondepartamental/selecciondepartamental.page';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import {ComponentModule} from '../../../components/component.module';
import { DepartamentosComponent } from 'src/app/components/departamentos/departamentos.component';
import { MapaService } from 'src/app/services/mapas/mapa.service';
import { NetworkService } from 'src/app/services/networks/network.service';
import { Favoritos } from 'src/app/interfaces/favoritos.interface';
import { Clinicas } from 'src/app/interfaces/clinicas.interface';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { firestore } from 'firebase';
import swal from 'sweetalert';
import { Alertas } from 'src/app/interfaces/alertas.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  usuario: Usuario;
  uid;
  session
  seccion="favoritos";
  clinicasCollection:AngularFirestoreCollection<Clinicas>;
  clnicasObserbavle:Observable<Clinicas[]>;
  clnicasObserbavle2:Observable<Clinicas[]>;
  clinicas=[];
  eventoVerificante: string;
  constructor(private route:Router,private actionSheet:ActionSheetController,private navController:NavController,public usuarioService: UsuarioService,public network:NetworkService,public mapasService:MapaService,private db:AngularFirestore,private auth:AngularFireAuth,private ModalController:ModalController,private alertController:AlertController) { 
    this.auth.auth.onAuthStateChanged(l=>{
      this.session=l.uid
      this.network.verificarConexion()
      this.cargarPerfil()

    
    
    
    })
   
  }

  ngOnInit() {
    this.traerFavoritos()
    this.traerClinicas()
    
  }

  cargarPerfil(){
    this.usuarioService.obtenerUsuarioEnSesion().subscribe(
      data =>{
        this.usuario = data;
        
        
      }
      
    );
   
  }

  async servicioDepartamento(uid)
  {

    this.db.collection('usuarios').doc(uid).snapshotChanges().subscribe(k=>{
      const datos=k.payload.data() as Usuario
    
      if(datos.municipio==null && datos.departamento==null){
       this.ejecucionDepartamental()
      }
    })
    
    
    
    
  }
async ejecucionDepartamental(){
  const data=await this.ModalController.create({
    component:DepartamentosComponent,
   
  })
  await data.present()
}


public cambioSegmento(evento) {
  this.seccion = evento.detail.value;
  if(this.seccion=='actividad' || this.seccion=='favoritos'){
    this.traerFavoritos()
    this.traerClinicas()
  }
}

public traerFavoritos(){
     this.db.collection('favoritos').doc(this.session).snapshotChanges().subscribe(p=>{
       const data=p.payload.data() as Favoritos
        
        this.clinicas=data.clinica
        this.eventoVerificante=data.uid
        console.log(this.clinicas)
           
      
     })
}

async traerClinicas(){
  this.clinicasCollection= this.db.collection('clinicas');
  this.clnicasObserbavle2 =this.clinicasCollection.snapshotChanges().pipe(map(actions => {
  return actions.map(action => {
  const data = action.payload.doc.data() as Clinicas;
  const id = action.payload.doc.id;
 
    return {id, ...data};
  });
  }));
}

reseteo(){ 
  this.navController.navigateForward(['/app/mapa'])
}

ejecutante(){
  this.traerFavoritos()
        this.traerClinicas()
}
async opcionesInterinas(nit,clini){
  const actionSheet = await this.actionSheet.create({
    header: 'Acciones',
    cssClass: 'my-custom-class',
    buttons: [{
      text: 'Comentarios',
      role: 'destructive',
      icon: 'chatbubbles',
      handler: () => {
        this.verComentarios(clini)
      }
    },
    {
      text: 'Trazar Ruta',
      icon: 'locate',
      role: 'destructive',
      handler: () => {
        this.mapasService.AbrirMetodico(nit,clini)
      }
    },

    
   
    {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present(); 
}
mapita(nit,clini){
  this.mapasService.AbrirMetodico(nit,clini)

}
quitarFavoritos(nit,clini){
 
 console.log(clini)
 console.log(nit)
  this.db.collection('clinicas',ref=>ref.where('nitie','==',nit)).doc(clini).update({
    favoritos:firestore.FieldValue.arrayRemove(this.session)
}).then(()=>{

  this.db.collection('favoritos').doc(this.session).update({
    clinica:firestore.FieldValue.arrayRemove(nit)

  }).then(()=>{
   
     swal('Clinica quitada','Ya no recibiras notificaciones','success')
    


  })
 
 
}).catch(()=>{
 this.network.verificarConexion()
})
}
async verComentarios(uidClini:string){
  var final=uidClini.trim()
  this.route.navigate(['/comentarios/',final])
}

async presentAlert() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Para que tengas en cuenta... ',
    subHeader: 'Información',
    message: 'R=Recomendada <br></br> Ss=Sinservicios <br></br> Cg=Congestionada',
    buttons: ['OK'],
    
  });

  await alert.present();
}


async serviciodeAlerta(uid){
  this.db.collection('alertas').doc('Zibopd8IIYZ4WYmFBIpb').snapshotChanges().subscribe(l=>{
    const data=l.payload.data() as Alertas
    if(data.usuario.includes(uid)){
 
      
    }else{
      this.presentAlertaIPS()
    }
  })
 
}


async presentAlertaIPS() {
  
  this.db.collection('favoritos').doc(this.session).snapshotChanges().subscribe(o=>{
    const data=o.payload.data() as Favoritos
    data.clinica.forEach(h=>{
      this.db.collection('clinicas',ref=>ref.where('nities','==',h)).snapshotChanges().subscribe(m=>{
        m.map(j=>{
          const data=j.payload.doc.data() as Clinicas
         if(data.actividad=='congestionada'){
            var mensaje="SE ENCUENTRA EN ALERTA DE CONGESTION"
            var nombre=data.nombres
            this.mensaje(data.uid,nombre,mensaje)
         }
        })
      })
    })
    
  })

}

async mensaje(uid,nombre,content){
    
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Para que tengas en cuenta...',
    subHeader: nombre,
    message: content,
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Okay',
        handler: () => {
          this.confirmada(uid)
        }
      }
    ]
    
  });

  await alert.present();
}

confirmada(uid){
  var cadena=uid+'_'+this.session
  this.db.collection('alertas').doc('Zibopd8IIYZ4WYmFBIpb').update({
    usuario:firestore.FieldValue.arrayUnion(cadena)
  })
}

}

