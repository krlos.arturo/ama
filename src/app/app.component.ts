import { Component } from '@angular/core';
import {OneSignal} from '@ionic-native/onesignal/ngx';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './services/auth/auth.service';
import { NetworkService } from './services/networks/network.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  uid
  appid={
    APP_ID:'d32c7def-d5a6-4e24-8f39-0f3aa2218694',
    ID:'189181957039'
  }
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private auth:AuthService,
    private authFire:AngularFireAuth,
    private oneSignal:OneSignal,
    private db:AngularFirestore,
    private networkService:NetworkService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {

      let language = this.translate.getBrowserLang();
      this.translate.setDefaultLang(language);
      this.statusBar.styleDefault();
      this.esconderSplash()
      this.setupPush()
      this.auth.reconocimientoRolPerfil()
      this.networkService.verificarConexion()
    });
  }
  setupPush() {
    // I recommend to put these into your environment.ts
    var iosSettings = {};
    iosSettings["kOSSettingsKeyAutoPrompt"] = false;
    iosSettings["kOSSettingsKeyInAppLaunchURL"] = false;
    
    this.oneSignal.startInit(this.appid.APP_ID,this.appid.ID);
    this.oneSignal.promptForPushNotificationsWithUserResponse();
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.setSubscription(true);
    
  
    this.oneSignal.getIds().then(g=>{

    this.authFire.auth.onAuthStateChanged(f=>{
    
      this.uid=f.uid
      this.db.collection('usuarios').doc(f.uid).update({
           oneSigID:g.userId,
           tokenSigID:g.pushToken
      })
    })
     
  })
  
 
    this.oneSignal.endInit();
  }
 

  esconderSplash(){
    setTimeout(() => {
      this.splashScreen.hide();
    }, 500);
  }



}
